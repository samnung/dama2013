#-------------------------------------------------
#
# Project created by QtCreator 2013-05-08T16:20:44
#
#-------------------------------------------------

QT       += core xml network
QT       -= gui

TARGET = dama2013-cli
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


HOSTNAME = $$system(hostname)

contains(HOSTNAME, "merlin.fit.vutbr.cz") {
	QMAKE_CXX	= g++-4.7
	QMAKE_CC	= $$QMAKE_CXX
	QMAKE_LINK	= $$QMAKE_CXX
}

macx {

	QMAKE_CXX	= g++-4.8
	QMAKE_CC	= $$QMAKE_CXX
	QMAKE_LINK	= $$QMAKE_CXX

	QMAKE_CXXFLAGS  += -mmacosx-version-min=10.7
}


greaterThan(QT_MAJOR_VERSION, 4): {
	QT += widgets
}


QMAKE_CXXFLAGS += -std=c++11


SOURCES += \
	src/model_tile.cpp \
	src/model_unit.cpp \
	src/model_player.cpp \
	src/model_move.cpp \
	src/model_global.cpp \
	src/model_board.cpp \
	src/model_aiplayer.cpp \
	src/cli_main.cpp \
    src/model_serverplayer.cpp \
    src/model_networkplayer.cpp

HEADERS += \
	src/model_tile.h \
	src/model_player.h \
	src/model_move.h \
	src/model_unit.h \
	src/model_global.h \
	src/model_board.h \
	src/model_aiplayer.h \
	src/cli_main.h \
    src/model_serverplayer.h \
    src/model_networkplayer.h
