
TARGET		= dama2013
BUILD_PATH	= build


UNAME := $(shell uname)
HOSTNAME := $(shell hostname)


PACK_NAME = ../xkrizr05.zip
LOGIN = xkrizr05
CID = 8645
ITEMS = 42053


# ------------ osetreni spousteni na Macu --------------
ifeq ($(UNAME), Darwin)
	RUN_FILE = ${BUILD_PATH}/${TARGET}.app/Contents/MacOS/${TARGET}
else
	RUN_FILE = ${BUILD_PATH}/${TARGET}
endif

# -------- QMAKE na merlinovi ------------------
ifeq (${HOSTNAME}, merlin.fit.vutbr.cz)
	QMAKE 	= /usr/local/share/Qt-4.7.3/bin/qmake
else
	QMAKE 	= qmake
endif


build: cli
	@mkdir -p ${BUILD_PATH}
	@cd ${BUILD_PATH} && ${QMAKE} ../${TARGET}.pro && make

run: build
	${RUN_FILE}

doxygen:
	mkdir -p doc
	doxygen doxy_config


cli:
	@mkdir -p ${BUILD_PATH}
	@cd ${BUILD_PATH} && ${QMAKE} ../${TARGET}-cli.pro && make

run_cli: cli
	${BUILD_PATH}/${TARGET}-cli


clean:
	rm -rf ${BUILD_PATH} doc



test:
	@cd unittests && bash test.sh


pack: ${PACK_NAME}

${PACK_NAME}:
	cd ../ && zip ${LOGIN}.zip ${LOGIN}/src/* ${LOGIN}/img/* ${LOGIN}/examples/* ${LOGIN}/*.qrc ${LOGIN}/*.pro ${LOGIN}/README.txt ${LOGIN}/Makefile ${LOGIN}/doxy_config --exclude \*.d \*.o *.gch


-include Global/global.mk


.PHONY: build run cli run_cli clean test doxygen ${PACK_NAME}
