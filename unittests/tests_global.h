/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: Třída představující instanci figurky na hrací ploše
 *
 * encoding: utf-8
 ******************************************************************************/

#include <iostream>

int errrrrrr = 0;
#define RET_CODE errrrrrr

#define PRINT(private,message) printf("TEST ERROR: %s[%d]: %s %s\n", __FILE__, __LINE__, private, message)

#define TEST_ASSERT_TRUE(condition,message) if (!condition) { PRINT("", message); errrrrrr = 1; }
#define TEST_NUMBER_EQUAL(number_1, number_2, message) if ( number_1 != number_2 ) { PRINT("number should be equal", message); errrrrrr = 1; }
#define TEST_ASSERT_FALSE(condition,message) if (condition) { PRINT("", message); errrrrrr = 1; }
