



CXXFLAGS		+= -std=c++11 -ggdb -MMD -I../../src -I../ -DTEST 

SOURCE_FOLDER	:= ${realpath ../../src}




ifneq (${DEP_CLASS}, )
	DEP_O = ${addsuffix .o, ${DEP_CLASS}}
	DEP_CPP = ${addprefix ${SOURCE_FOLDER}/, ${addsuffix .cpp, ${DEP_CLASS}}}
endif


UNIT_O			:= ${UNIT}.o
TEST_O 			:= ${TEST}.o
UNIT_CPP		:= ${SOURCE_FOLDER}/${UNIT}.cpp
TEST_CPP		:= ${TEST}.cpp

DEP				:= ${wildcard *.d}
-include ${DEP}



test: ${TEST}

${TEST}: ${UNIT_O} ${TEST_O} ${DEP_O}
	${CXX} ${LDFLAGS} -o $@ $^


${TEST_O}: ${TEST_CPP}
	${CXX} ${CXXFLAGS} -c -o $@ $<

${UNIT_O}: ${UNIT_CPP}
	${CXX} ${CXXFLAGS} -c -o $@ $<

${DEP_O}: ${DEP_CPP}
	${CXX} ${CXXFLAGS} -c $^

