#!/bin/bash

SUCC=''


for test in test_*; do
	cd $test

	echo "-------------- start $test"

	make test > /dev/null && ./$test

	if [[ $? -ne 0 ]]; then
		SUCC='1'
	fi

	echo "---------------- end $test"

	cd - > /dev/null
done



if [[ $SUCC -ne '' ]]; then
	printf "\nTEST FAILED\n"
else
	printf "\nTEST SUCCESS\n"
fi
