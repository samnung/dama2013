/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: Třída představující instanci figurky na hrací ploše
 *
 * encoding: utf-8
 ******************************************************************************/

#include <iostream>

#include "model_board.h"
#include "tests_global.h"

#define TEST_BOARD_VALID(__board,__x,__y) TEST_ASSERT_TRUE(__board.tile(__x, __y)->isOccupied(), "Na pozici "  #__x  #__y  " by měla být figurka.");
#define TEST_BOARD_INVALID(__board,__x,__y) TEST_ASSERT_FALSE(__board.tile(__x, __y)->isOccupied(), "Na pozici "  #__x  #__y  " by neměla být figurka.");


Coordinates  remTest;

void testValidInitPositions(Board & board)
{
	TEST_BOARD_VALID(board, 0, 0);
	TEST_BOARD_VALID(board, 0, 2);
	TEST_BOARD_VALID(board, 1, 1);
	TEST_BOARD_VALID(board, 2, 0);
	TEST_BOARD_VALID(board, 2, 2);
	TEST_BOARD_VALID(board, 3, 1);
	TEST_BOARD_VALID(board, 4, 0);
	TEST_BOARD_VALID(board, 4, 2);
	TEST_BOARD_VALID(board, 5, 1);
	TEST_BOARD_VALID(board, 6, 0);
	TEST_BOARD_VALID(board, 6, 2);
	TEST_BOARD_VALID(board, 7, 1);

	TEST_BOARD_VALID(board, 0, 6);
	TEST_BOARD_VALID(board, 1, 7);
	TEST_BOARD_VALID(board, 1, 5);
	TEST_BOARD_VALID(board, 2, 6);
	TEST_BOARD_VALID(board, 3, 7);
	TEST_BOARD_VALID(board, 3, 5);
	TEST_BOARD_VALID(board, 4, 6);
	TEST_BOARD_VALID(board, 5, 7);
	TEST_BOARD_VALID(board, 5, 5);
	TEST_BOARD_VALID(board, 6, 6);
	TEST_BOARD_VALID(board, 7, 7);
	TEST_BOARD_VALID(board, 7, 5);
}

void testInvalidInitPositions(Board & board)
{
	Tiles tiles = board.tiles();

	TEST_BOARD_INVALID(board, 0, 5);
	TEST_BOARD_INVALID(board, 0, 7);
	TEST_BOARD_INVALID(board, 1, 6);
	TEST_BOARD_INVALID(board, 2, 5);
	TEST_BOARD_INVALID(board, 2, 7);
	TEST_BOARD_INVALID(board, 3, 6);
	TEST_BOARD_INVALID(board, 4, 5);
	TEST_BOARD_INVALID(board, 4, 7);
	TEST_BOARD_INVALID(board, 5, 6);
	TEST_BOARD_INVALID(board, 6, 5);
	TEST_BOARD_INVALID(board, 6, 7);
	TEST_BOARD_INVALID(board, 7, 6);

	TEST_BOARD_INVALID(board, 0, 1);
	TEST_BOARD_INVALID(board, 1, 2);
	TEST_BOARD_INVALID(board, 1, 0);
	TEST_BOARD_INVALID(board, 2, 1);
	TEST_BOARD_INVALID(board, 3, 2);
	TEST_BOARD_INVALID(board, 3, 0);
	TEST_BOARD_INVALID(board, 4, 1);
	TEST_BOARD_INVALID(board, 5, 2);
	TEST_BOARD_INVALID(board, 5, 0);
	TEST_BOARD_INVALID(board, 6, 1);
	TEST_BOARD_INVALID(board, 7, 2);
	TEST_BOARD_INVALID(board, 7, 0);

	for (int i = 0; i < BOARD_WIDTH; ++i)
	{
		for (int j = 3; j < 5; ++j)
		{
			TEST_BOARD_INVALID(board, i, j);
		}
	}
}

void testValidMoves(Board & board)
{
	TEST_ASSERT_TRUE(board.makeMove(Coordinates(0, 2), Coordinates(1, 3),&remTest), "A3 -> B4 je validní tah.");
	TEST_ASSERT_TRUE(board.makeMove(Coordinates(3, 5), Coordinates(2, 4),&remTest), "D6 -> C5 je validní tah.");

	TEST_ASSERT_FALSE(board.makeMove(Coordinates(1, 3), Coordinates(0, 4),&remTest), "B4 -> A5 neni validní tah.");
	TEST_ASSERT_FALSE(board.makeMove(Coordinates(2, 4), Coordinates(2, 4), &remTest), "from = to.");
}

void testInvalidMoves(Board & board)
{
	TEST_ASSERT_FALSE(board.canMakeMove(Coordinates(1, 1), Coordinates(3, 3)), "B2 -> D4 je nevalidní tah.");
	TEST_ASSERT_FALSE(board.canMakeMove(Coordinates(2, 2), Coordinates(5, 5)), "C3 -> E5 je nevalidní tah.");
	TEST_ASSERT_FALSE(board.canMakeMove(Coordinates(2, 2), Coordinates(5, 5)), "C3 -> E5 je nevalidní tah.");
}
//void testTilesToBeTaken(Board & board)

//}

int main (int argc, char ** argv)
{
	Board board;
	testValidInitPositions(board);
	testInvalidInitPositions(board);

	testValidMoves(board);
	testInvalidMoves(board);

	std::cout << board;

	return RET_CODE;

}
