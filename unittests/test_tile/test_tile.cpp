/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: Třída představující instanci figurky na hrací ploše
 *
 * encoding: utf-8
 ******************************************************************************/

#include <iostream>

#include "model_tile.h"
#include "tests_global.h"



int main (int argc, char ** argv)
{
	Tile tile(1, 1, ColorBlack, new Unit(ColorWhite, Unit::Stone));

	TEST_ASSERT_TRUE(tile.isOccupied(), "Políčko by mělo mít na sobě figurku.");

	Unit *unit = tile.removeUnit();

	TEST_ASSERT_TRUE(unit, "Figurka musí existovat.");
	TEST_ASSERT_FALSE(tile.isOccupied(), "Políčko by mělo být prázdné.");

	delete unit;

	return RET_CODE;
}
