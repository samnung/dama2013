/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: Třída představující instanci figurky na hrací ploše
 *
 * encoding: utf-8
 ******************************************************************************/

#include <iostream>

#include "model_unit.h"
#include "tests_global.h"

void testQueenValidMove()
{
	Unit unit(ColorBlack, Unit::Queen);
	TEST_ASSERT_TRUE(unit.canMoveTo(1, 1), "Dama by měla být schopna jít kamkoliv po diagonale.");
	TEST_ASSERT_TRUE(unit.canMoveTo(2, 2), "Dama by měla být schopna jít kamkoliv po diagonale.");
	TEST_ASSERT_TRUE(unit.canMoveTo(-1, -1), "Dama by měla být schopna jít kamkoliv po diagonale.");
	TEST_ASSERT_TRUE(unit.canMoveTo(10, 10), "Dama by měla být schopna jít kamkoliv po diagonale.");
	TEST_ASSERT_TRUE(unit.canMoveTo(4, 4), "Dama by měla být schopna jít kamkoliv po diagonale.");
	TEST_ASSERT_TRUE(unit.canMoveTo(-4, 4), "Dama by měla být schopna jít kamkoliv po diagonale.");
	TEST_ASSERT_TRUE(unit.canMoveTo(-3, -3), "Dama by měla být schopna jít kamkoliv po diagonale.");
	TEST_ASSERT_TRUE(unit.canMoveTo(4, -4), "Dama by měla být schopna jít kamkoliv po diagonale.");

}


void testQueenInvalidMove()
{
	Unit unit(ColorBlack, Unit::Queen);
	TEST_ASSERT_FALSE(unit.canMoveTo(1, 10), "Dáma nesmí jít kamkoliv.");
	TEST_ASSERT_FALSE(unit.canMoveTo(-2, 3), "Dáma nesmí jít kamkoliv.");
	TEST_ASSERT_FALSE(unit.canMoveTo(-1, -10), "Dáma nesmí jít kamkoliv.");
	TEST_ASSERT_FALSE(unit.canMoveTo(10, 1), "Dáma nesmí jít kamkoliv.");
	TEST_ASSERT_FALSE(unit.canMoveTo(4, 3), "Dáma nesmí jít kamkoliv.");
}


void testStoneValidMove()
{
	Unit unit(ColorBlack, Unit::Stone);
	TEST_ASSERT_TRUE(unit.canMoveTo(1, 1), "Kámen by měl jít dopředu po diagonále.");
	TEST_ASSERT_TRUE(unit.canMoveTo(-1, 1), "Kámen by měl jít dopředu po diagonále.");
}

void testStoneInvalidMove()
{
	Unit unit(ColorBlack, Unit::Stone);
	TEST_ASSERT_FALSE(unit.canMoveTo(0, 1), "Kámen by neměl být schopen jit kamkoliv.");
	TEST_ASSERT_FALSE(unit.canMoveTo(2, 2), "Kámen by neměl být schopen jit kamkoliv.");
	TEST_ASSERT_FALSE(unit.canMoveTo(-1, -1), "Kámen by neměl být schopen jit kamkoliv.");
	TEST_ASSERT_FALSE(unit.canMoveTo(10, 10), "Kámen by neměl být schopen jit kamkoliv.");
	TEST_ASSERT_FALSE(unit.canMoveTo(4, 4), "Kámen by neměl být schopen jit kamkoliv.");
	TEST_ASSERT_FALSE(unit.canMoveTo(-4, 4), "Kámen by neměl být schopen jit kamkoliv.");
	TEST_ASSERT_FALSE(unit.canMoveTo(-3, -3), "Kámen by neměl být schopen jit kamkoliv.");
	TEST_ASSERT_FALSE(unit.canMoveTo(4, -4), "Kámen by neměl být schopen jit kamkoliv.");
}

int main (int argc, char ** argv)
{
	testQueenValidMove();
	testQueenInvalidMove();

	testStoneValidMove();
	testStoneInvalidMove();

	return RET_CODE;
}
