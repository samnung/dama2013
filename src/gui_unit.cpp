/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: metody GUI pro práci s hrací jednotkou
 *
 * encoding: utf-8
 ******************************************************************************/

#include "gui_unit.h"

#include <QPainter>
#include <QPaintEvent>
#include <QImage>

const QString MIME_DATA = "others/dama2013-unit";
static QPixmap * DAMA_BLACK_PIXMAP = NULL;
static QPixmap * DAMA_WHITE_PIXMAP = NULL;


const qreal UNIT_SCALE = 0.8;
const qreal DAMA_SCALE = 0.7;


const QString & GUnit::mimeData()
{
	return MIME_DATA;
}

GUnit::GUnit(const Unit *unit, QWidget *parent) : QWidget(parent), Unit(*unit)
{
	if ( ! DAMA_BLACK_PIXMAP )
	{
		DAMA_BLACK_PIXMAP = new QPixmap(":/img/dama_black.png");
		DAMA_WHITE_PIXMAP = new QPixmap(":/img/dama_white.png");
	}
}



void GUnit::updateBy(Unit *unit)
{
	setType(unit->type());
	setColor(unit->color());
	update();
}



void GUnit::paintEvent(QPaintEvent *e)
{
	QPainter painter;
	painter.begin(this);

	painter.setBackgroundMode(Qt::TransparentMode);
	painter.setRenderHints(QPainter::Antialiasing);


	qreal width = e->rect().width();
	width /= 2;
	width *= UNIT_SCALE;

	int width_int = static_cast<int>(width);


	QPixmap * dama_pixmap = NULL;
	QRadialGradient grad = QRadialGradient(QPointF(e->rect().width()/2, e->rect().width()/2), width);

	if (color() == ColorBlack)
	{
		painter.setPen(QPen(Qt::white, width / 10));

		grad.setColorAt(0, QColor(95, 95, 95, 255));
		grad.setColorAt(1, QColor(30, 30, 30, 255));
		painter.setBrush(grad);

		dama_pixmap = DAMA_WHITE_PIXMAP;
	}
	else
	{
		painter.setPen(QPen(Qt::black, width / 10));

		grad.setColorAt(0, QColor(130, 130, 130, 255));
		grad.setColorAt(1, QColor(225, 225, 225, 255));
		painter.setBrush(grad);

		dama_pixmap = DAMA_BLACK_PIXMAP;
	}

	painter.drawEllipse(e->rect().center(), width_int, width_int);



	if ( type() == Unit::Stone )
	{
		width_int = static_cast<int>(width/4);
		painter.drawEllipse(e->rect().center(), width_int, width_int);
	}
	else
	{
		width = e->rect().width();
		width /= 2;
		width *= DAMA_SCALE;

		QRect rect = e->rect();
		rect.moveTo(rect.width()/2 - width, rect.width()/2 - width);

		QSizeF size = QSizeF(rect.size());
		size.rheight() *= DAMA_SCALE;
		size.rwidth() *= DAMA_SCALE;
		rect.setSize(size.toSize());

		painter.drawPixmap(rect, *dama_pixmap);

	}

	painter.end();
}


QString GUnit::toString() const
{
	std::stringstream ss;
	ss << *this;
	return QString(ss.str().c_str());
}

GUnit * GUnit::fromString(const QString & string, QWidget * parent)
{
	Unit * unit = Unit::fromString(string.toStdString());
	return new GUnit(unit, parent);
}

QPixmap GUnit::toPixmap()
{
	QImage image(size(), QImage::Format_ARGB32);
	image.fill(Qt::transparent);
	this->render(&image, QPoint(), QRegion(), IgnoreMask);
	return QPixmap::fromImage(image);
}

