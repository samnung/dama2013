/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: implementace GUI hrací desky
 *
 * encoding: utf-8
 ******************************************************************************/

#include "gui_board.h"

#include "model_global.h"
#include "model_board.h"

#include "gui_unit.h"

#include <QResizeEvent>
#include <QPainter>
#include <QMessageBox>
#include <QTimer>





const qreal BOARD_MARGIN = 0.1;



QRectF boardRect(const QRectF & frameRect)
{
	qreal space = frameRect.height() * BOARD_MARGIN;
	space /= 2;
	QRectF rect = QRectF(frameRect);
	rect.adjust(space, space, -space, -space);

	return rect;
}

QRectF verticalLabel(int number, const QRectF & boardRect)
{
	qreal x, y, height, width;

	width = (boardRect.width() / (1 - BOARD_MARGIN / 2)) - boardRect.width();
	height = boardRect.height() / BOARD_WIDTH;

	x = boardRect.left() - width;
	y = boardRect.top() + height * number;


	return QRectF(x, y, width, height);
}

QRectF horizontalLabel(int number, const QRectF & boardRect)
{
	qreal x, y, height, width;

	width = boardRect.width() / BOARD_WIDTH;
	height = (boardRect.height() / (1 - BOARD_MARGIN / 2)) - boardRect.height();


	x = boardRect.left() + width * number;
	y = boardRect.top() + boardRect.height();


	return QRectF(x, y, width, height);
}

QRectF frameRect(const QRectF & rect)
{
	qreal x, y, height, width;

	height = MIN(rect.height(), rect.width());
	width = MAX(rect.height(), rect.width());

	qreal space = height * BOARD_MARGIN;

	height -= space;

	space /= 2;

	if ( rect.height() < rect.width() )
	{
		x = rect.x() + width / 2 - height / 2;
		y = rect.y() + space;
	}
	else
	{
		x = rect.x() + space;
		y = rect.y() + width / 2 - height / 2;
	}

	return QRectF(x, y, height, height);
}

QRectF tileRect(Coordinate x, Coordinate y, QRectF in)
{
	qreal width = in.width() / BOARD_WIDTH;

	qreal _x = in.x() + (width * x);
	qreal _y = in.y() + (width * (BOARD_WIDTH - 1 - y));
	return QRectF(_x, _y, width, width);
}










GBoard::GBoard(const Board &board, AbstractController * controller, QWidget *parent)
	: QWidget(parent)
{
	_movingUnit = false;
	_showHints = false;

	_controller = controller;

	_fromTile = _toTile = NULL;

	_frame.setParent(this);
	_frame.setStyleSheet("background-color: #aaa;");


	// ------------- inicializace vertikálních souřadnic -----------
	for ( char i = BOARD_WIDTH; i > 0; i-- )
	{
		QLabel *label = new QLabel( QString('0'+i), this);
		label->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
		_vertical.push_back(label);
	}

	// ------------- inicializace horizontálních souřadnic -----------
	for ( char i = 0; i < BOARD_WIDTH; i++ )
	{
		QLabel *label = new QLabel( QString('A'+i), this);
		label->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
		_horizontal.push_back(label);
	}

	// ------------------ přidání políček na desku ------------------
	for (Coordinate i = 0; i < BOARD_WIDTH; ++i)
	{
		for (Coordinate j = 0; j < BOARD_WIDTH; ++j)
		{
			GTile * tile = new GTile(board.tile(i, j), this);

			connect(tile, SIGNAL(tileDidClicked(GTile*,bool&)), this, SLOT(tileWillSelect(GTile*,bool&)));

			connect(tile, SIGNAL(unitWillPickUp(GTile*,bool&)), this, SLOT(unitWillPickUp(GTile*,bool&)));
			connect(tile, SIGNAL(unitDidPickUp(GTile*)), this, SLOT(unitDidPickUp(GTile*)));
			connect(tile, SIGNAL(unitWillPutDown(GTile*,bool&)), this, SLOT(unitWillPutDown(GTile*,bool&)));

			_tiles.push_back(tile);
		}
	}
}






void GBoard::resizeEvent(QResizeEvent *)
{
	QRectF _frameRect = frameRect(rect());

	_frame.setGeometry(_frameRect.toAlignedRect());

	QRectF board = boardRect(_frameRect);

	int size = verticalLabel(0, board).toRect().height() / 3;
	QFont font = _vertical[0]->font();
	font.setPixelSize(size);

	int i = 0;
	for (QLabel * label : _vertical)
	{
		label->setGeometry(verticalLabel(i, board).toAlignedRect());
		label->setFont(font);
		i++;
	}

	i = 0;
	for (QLabel * label : _horizontal)
	{
		label->setGeometry(horizontalLabel(i, board).toAlignedRect());
		label->setFont(font);
		i++;
	}


	// --------- aktualizace rozložení políček ------------
	for (Coordinate y = 0; y < BOARD_WIDTH; y++)
	{
		for (Coordinate x = 0; x < BOARD_WIDTH; x++)
		{
			_tiles[mapFunction(x, y)]->setGeometry(tileRect(x, y, board).toAlignedRect());
		}
	}
}


void GBoard::makeMove(Coordinates from, Coordinates to)
{
	GTile * fromTile = _tiles.at(mapFunction(from));
	GTile * toTile = _tiles.at(mapFunction(to));

	toTile->setUnit(fromTile->removeUnit());

	update();
}

void GBoard::removeUnit(Coordinates from)
{
	GTile * fromTile = _tiles.at(mapFunction(from));
	GUnit * unit = fromTile->removeUnit();

	unit->deleteLater();
}

void GBoard::addUnit(Coordinates to, Unit *unit)
{
	GTile * fromTile = _tiles.at(mapFunction(to));
	fromTile->setUnit(new GUnit(unit, fromTile));
}
void GBoard::updateUnit(Coordinates where, Unit *by)
{
	_updateUnitWhere = where;
	_updateUnitBy = by;

	QTimer::singleShot(0, this, SLOT(_updateUnit()));
}
void GBoard::_updateUnit()
{
	GTile * fromTile = _tiles.at(mapFunction(_updateUnitWhere));
	fromTile->unit()->updateBy(_updateUnitBy);
}


/**
 * Zpracování klikacích událostí nad políčkem
 * @param tile		kliknuté políčko
 */
void GBoard::tileWillSelect(GTile *tile, bool & validity)
{
	validity = false;

	if ( tile->isSelected() )
	{
		if ( ! _fromTile )
		{
			if ( ! _controller->tilesWhereCanMove(tile->coordinates()).empty() )
			{
				_fromTile = tile;
				validity = true;
				_movingUnit = true;
				updateHighlights();
			}
			else
				return;

		}
		else if ( ! _toTile )
		{
			if ( _fromTile == tile or tile->isOccupied() )
				return;

			_toTile = tile;

			bool valid = false;
			_controller->boardWillMakeMove(_fromTile->coordinates(), _toTile->coordinates(), valid);

			// ----------- proveď přesun --------------------
			if ( valid )
			{
				GUnit * unit = _fromTile->removeUnit();
				_toTile->setUnit(unit);

				_fromTile->setSelected(false);
				_fromTile = NULL;

				_movingUnit = false;
				setAllTilesUnselected();
				updateHighlights();

				validity = true;
			}

			_toTile = NULL;
		}
	}
	// odznačení políčka
	else
	{
		validity = true;

		if ( _fromTile )
		{
			_fromTile = NULL;

			_movingUnit = false;
			updateHighlights();
		}
		else if ( _toTile )
		{
			_toTile = NULL;
		}

		return;
	}
}


void GBoard::highlightTiles(Tiles tiles, QColor color)
{
	for ( Tile * tile : tiles )
	{
		GTile * gtile = _tiles.at(mapFunction(tile->coordinates()));
		gtile->setColorHighlight(color);
	}
}


void GBoard::setShowHints(bool showHints)
{
	_showHints = showHints;

	updateHighlights();
}

void GBoard::updateHighlights()
{
	// --------- všechny odznačit -------------
	for ( GTile * gtile : _tiles )
	{
		gtile->setColorHighlight();
	}

	if ( _showHints )
	{
		// --------- označit jen určité -------------
		Tiles tilesToSelect;
		QColor colorToSelect = QColor(0, 220, 0, 150);

		if ( _movingUnit )
		{
			tilesToSelect = _controller->tilesWhereCanMove(_fromTile->coordinates());
		}
		else
		{
			tilesToSelect = _controller->tilesWithPossibleMove();
		}

		highlightTiles(tilesToSelect, colorToSelect);
	}
}

void GBoard::setAllTilesUnselected(bool selected)
{
	for ( GTile * gtile : _tiles )
	{
		gtile->setSelected(selected);
	}
}

void GBoard::unitWillPickUp(GTile *self, bool &validity)
{
	Tiles tiles = _controller->tilesWhereCanMove(self->coordinates());
	validity = !tiles.empty();
}

void GBoard::unitDidPickUp(GTile * tile)
{
	_movingUnit = true;
	_fromTile = tile;

	setAllTilesUnselected();
	updateHighlights();
}

void GBoard::unitWillPutDown(GTile * tile, bool & validity)
{
	_controller->boardWillMakeMove(_fromTile->coordinates(), tile->coordinates(), validity);

	_movingUnit = false;

	_toTile = _fromTile = NULL;

	updateHighlights();
}
