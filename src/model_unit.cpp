/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05), Vojtěch Dvořáček (xdvora0y)
 *
 * Popis: Třída představující instanci figurky na hrací ploše
 *
 * encoding: utf-8
 ******************************************************************************/

#include <iostream>

#include "model_unit.h"



bool Unit::canMoveTo(Coordinate x, Coordinate y, bool taking)
{
	switch ( _type )
	{
		case Queen:
			return (abs(x) == abs(y));
			break;
		case Stone:
            if( taking )
                return  (abs(x) == 2 and y == 2);
            else
                return (abs(x) == 1 and y == 1);
			break;
	}

	return false;
}

bool Unit::canMoveTo(Coordinates xy, bool taking)
{
    return this->canMoveTo(xy.x, xy.y, taking);
}




std::ostream & operator << (std::ostream & os, const Unit & unit)
{
	switch ( unit.type() )
	{
		case Unit::Stone:
			os << ' ' << colorToString(unit.color()) << ' ';
			break;
		case Unit::Queen:
			os << '*' << colorToString(unit.color()) << '*';
			break;
	}
	return os;
}

Unit * Unit::fromString(const std::string & string)
{
	Unit::Type type;
	Color color;
	if ( string.find("*") == 0 and string.find("*", 1) == 2 )
		type = Unit::Queen;
	else
		type = Unit::Stone;

	if ( string.find(ColorBlackString) == 1 )
		color = ColorBlack;
	else
		color = ColorWhite;

	return new Unit(color, type);
}
