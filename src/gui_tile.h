/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: třída popisující GUI políčko
 *
 * encoding: utf-8
 ******************************************************************************/

#ifndef GTILE_H
#define GTILE_H

#include <QWidget>
#include "model_tile.h"

class GUnit;

/**
 * @class GTile
 *
 * Třída pro popis grafické políčka.
 *
 * Obstarává vykreslování a zvýraznění políčka + případné vykreslení figurky.
 *
 * Zajištuje práci s Drag & Drop, případně klikací mechanismus pro přesun figurek.
 *
 * Dědí z Tile, je kvůli společným vlastnostem, do budoucna by se možná mohla použít grafická
 * reprezentace pro modelovou část a nemusely by se synchronizovat dvě desky.
 *
 * Metody setUnit(), removeUnit(), unit() a isOccupied() jsou zde definovány z důvodu neznalosti fungování jazyka při použití dědičnosti a času.
 */
class GTile : public QWidget, public Tile
{
	Q_OBJECT

	// figurka
	GUnit * _unit;

	// zvýraznění
	bool _selected;
	QColor _highlightColor;

	/*
	 * Proměnné pro práci s událostmi z myši, apod
	 */
	bool _drag;
	QPoint _mouseLastClick;
	bool _mouseClick;


public:
	/**
	 * Konstruktor políčka
	 * @param tile		modelové políčko, podle kterého se čerpají informace (barva/souřadnice)
	 * @param parent	QWidget parent
	 */
	explicit GTile(const Tile * tile, QWidget *parent = 0);

	/**
	 * Je políčko označeno ?
	 * @return	true == označeno
	 */
	bool isSelected();

	/**
	 * Nastavuje stav políčka, defaultně na true
	 *
	 * Pro označení se používá defaultní barva, pro označení vlastní barvou použijte funkci setColorHighlight()
	 *
	 * volá update()
	 *
	 * @param isSelected	stav označení
	 */
	void setSelected(bool isSelected = true);

	/**
	 * Nastvuje barvu zvýraznění
	 *
	 * Stav označení má větší prioritu, viz setSelected()
	 *
	 * volá update()
	 *
	 * @param color		barva, kterou se políčko zvýrazní
	 */
	void setColorHighlight(QColor color = QColor());

	/**
	 * Přepis metody pro nastavení nesouci figurky
	 *
	 * volá update()
	 *
	 * @param unit	figurka, kterou chceme vložit na políčko
	 */
	void setUnit(GUnit *unit);


	/**
	 * Přepis metody pro odstranění figurky z políčka
	 *
	 * volá update()
	 *
	 * @return	ukazatel na figurku, kterou odstranil
	 */
	GUnit * removeUnit();

	/**
	 * Přepis metody pro vracení figurky
	 *
	 * @return	ukazatel na figurku, která je právě na políčku
	 */
	GUnit * unit() const { return _unit; }

	/**
	 * Přepis metody pro zjištění
	 * @return
	 */
	bool isOccupied() const { return _unit != NULL; }


protected:
	// paint events
	void paintEvent(QPaintEvent *e);
	void resizeEvent(QResizeEvent *);

	// drag events
	void dragEnterEvent(QDragEnterEvent *e);
	void dragLeaveEvent(QDragLeaveEvent *e);
	void dragMoveEvent(QDragMoveEvent *e);
	void dropEvent(QDropEvent *e);

	// mouse events
	void mousePressEvent(QMouseEvent *e);
	void mouseMoveEvent(QMouseEvent *e);
	void mouseReleaseEvent(QMouseEvent *e);


private:
	void updateUnitGeometry();

signals:

	/**
	 * Vyvolá se při kliknutí myší na políčko
	 *
	 * Pokud se vrátí validity == false, políčko se neoznačí
	 *
	 * @param self		ukazatel na sebe, pro zjištění více informací
	 * @param validity	zpětná vazba pro validaci
	 */
	void tileDidClicked(GTile *self, bool & validity);

	/**
	 * Vyvolá se při pokusu o zvednutí figurky
	 *
	 * Pokud se vrátí validity == false, uživatel nedokáže zvednout figurku
	 *
	 * @param self		ukazatel na sebe, pro zjištění více informací
	 * @param validity	zpětná vazba pro validaci
	 */
	void unitWillPickUp(GTile *self, bool & validity);

	/**
	 * Vyvolá se při zvednutí figurky
	 *
	 * Tento signál, zde nemusí být, stačí signál unitWillPickUp()
	 *
	 * @param self	ukazatel na sebe, pro zjištění více informací
	 */
	void unitDidPickUp(GTile *self);

	/**
	 * Vyvolá se při upuštění figurky na políčko
	 *
	 * Pokud se vrátí validity == false, figurka se navrátí na své původní políčko
	 *
	 * @param self		ukazatel na sebe, pro zjištění více informací
	 * @param validity	zpětná vazba pro validaci
	 */
	void unitWillPutDown(GTile *self, bool & validity);

};

#endif // GTILE_H
