/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05), Vojtěch Dvořáček (xdvora0y)
 *
 * Popis: implementace metod tridy Move pro praci s pohybem
 *
 * encoding: utf-8
 ******************************************************************************/

#include "model_move.h"
#include <QDataStream>

#include <QDomDocument>
#include <QDomElement>
#include <QDomNode>
#include <QDomAttr>
#include <QDebug>



/* **********  	Move    *********** */

std::ostream & operator << (std::ostream & os, const Move & move)
{
	os << move._from;
	os << (move._taken.isNull() ? "-" : "x");
	os << move._to;

	return os;
}

QString Move::toString() const
{
	std::stringstream ss;
	ss << *this;
	return QString(ss.str().c_str());
}

QByteArray Move::toNetworkFormat()
{
	QByteArray array;

	array.append(_from.toNetworkFormat());
	array.append(_to.toNetworkFormat());

	return array;
}

Move * Move::fromNetworkFormat(QByteArray &array)
{
	Move * move = new Move;

	move->_from = Coordinates::fromNetworkFormat(array);
	move->_to = Coordinates::fromNetworkFormat(array);

	return move;
}

QDomElement Move::toXML(QDomDocument &doc)
{
	QDomElement element = doc.createElement("move");
	element.setAttribute("from", _from.toXMLValue());
	element.setAttribute("to", _to.toXMLValue());
	element.setAttribute("taken", _taken.toXMLValue());
	return element;
}

Move * Move::fromXML(const QDomElement &element)
{
	Move * move = NULL;
	if ( element.tagName() == "move" )
	{
		move = new Move;
		move->_from = Coordinates::fromXMLValue(element.attribute("from"));
		move->_to = Coordinates::fromXMLValue(element.attribute("to"));
		move->_taken = Coordinates::fromXMLValue(element.attribute("taken"));

	}

	return move;
}

QByteArray Move::toStandardFormat()
{
	QByteArray array;

	array.append(toString());

	return array;
}

Move * Move::fromStandardFormat(QByteArray &array)
{
	Move * move = new Move;

	// ------ načtení počáteční souřadnice ---------
	move->_from = Coordinates::fromStandardFormat(array);

	// ------- kontrola znaku mezi souřadnicemi -----------
	if ( array.at(0) != '-' and array.at(0) != 'x' )
	{
		delete move;
		return NULL;
	}

	array.remove(0, 1);

	// ------ načtení koncové souřadnice ---------
	move->_to = Coordinates::fromStandardFormat(array);

	return move;
}






/* **************************************************************************
 *								 Moves
 * ************************************************************************** */
std::ostream & operator << (std::ostream & os, Moves & moves)
{
	for ( Move * move : moves )
	{
		os << *move << " ";
	}

	return os;
}


QByteArray Moves::toNetworkFormat()
{
	QByteArray array;

	QDataStream data(&array, QIODevice::WriteOnly);
	data.setByteOrder(QDataStream::LittleEndian);


	data << static_cast<quint32>(size());


	for ( Move * move : *this )
	{
		array.append(move->toNetworkFormat());
	}

	return array;
}

Moves Moves::fromNetworkFormat(QByteArray & array)
{
	Moves moves;

	QDataStream data(&array, QIODevice::ReadOnly);
	data.setByteOrder(QDataStream::LittleEndian);

	quint32 size = 0;

	data >> size;

	array.remove(0, 4);

	for (quint32 i = 0; i < size; i++)
	{
		Move *move = Move::fromNetworkFormat(array);
		moves.push_back(move);
	}

	return moves;
}


QDomElement Moves::toXML(QDomDocument &doc)
{
	QDomElement movesRootElement = doc.createElement("moves");

	int i = 0;
	for ( Move * move : *this )
	{
		QDomElement element = move->toXML(doc);
		element.setAttribute("order", i);

		i++;
		movesRootElement.appendChild(element);
	}

	return movesRootElement;
}


Moves Moves::fromXML(const QDomElement &element)
{
	Moves moves;

	if ( element.tagName() == "moves" )
	{
		QDomNodeList list = element.childNodes();
		for ( int i = 0; i < list.size(); i++ )
		{
			QDomElement moveElement = list.at(i).toElement();

			Move *move = Move::fromXML(moveElement);

			moves.push_back(move);
		}
	}

	return moves;
}

QByteArray Moves::toStandardFormat()
{
	QByteArray array;

	for ( size_t i = 0; i < size(); i++ )
	{
		QString string;

		string += QString("%1. ").arg((i/2)+1);

		Move * move = at(i);

		string += move->toString();

		i++;
		if ( size() > i )
		{
			move = at(i);
			string += " " + move->toStandardFormat();
		}

		string += "\n";

		array += string;
	}

	return array;
}

Moves Moves::fromStandardFormat(const QByteArray &array)
{
	QByteArray copy = array;
	Moves moves;

	while ( ! copy.isEmpty() )
	{
		if ( isspace(copy.at(0)) )
			copy.remove(0, 1);
		else
		{
			if ( isdigit(copy.at(0)) )
			{
				int pos = copy.indexOf('.');

				if ( pos != -1 )
				{
					copy.remove(0, pos + 2); // <number>.<space>

					Move * move = Move::fromStandardFormat(copy);

					if ( move )
					{
						moves.push_back(move);

						copy.remove(0, 1); // <space>

						move = Move::fromStandardFormat(copy);
						if ( move )
							moves.push_back(move);

					}
					else
						return Moves();

				}
				else
					return Moves();
			}
		}
	}

	return moves;
}
