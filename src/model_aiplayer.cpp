/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: implementace vyberu tahu pro AI
 *
 * encoding: utf-8
 ******************************************************************************/

#include "model_aiplayer.h"
#include "model_abstractcontroller.h"
#include "model_board.h"

#include <QTimer>


AIPlayer::AIPlayer(const QString & name, AbstractController * controller, QObject *parent)
	: Player(name, controller, parent)
{

}


void AIPlayer::moveWasPerformed(Coordinates, Coordinates)
{
	// ------- náhodná prodleva pro odpověď ----------
	QTimer::singleShot(randomNumber(500, 2000), this, SLOT(_makeMove()));
}


void AIPlayer::_makeMove()
{
	// zíksání seznamu možných tahů
	Moves moves = _controller->possibleMovesWithUnitColor(color());

	if ( moves.empty() )
		return;

	// jádro AI, náhodný výber tahu
	Move * move = moves.at(randomNumber(0, moves.size() - 1));

	emit makesMove(this, move->from(), move->to());
}
