/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: implementace herních operací nad GUI
 *
 * encoding: utf-8
 ******************************************************************************/

#include "gui_mainwindow.h"
#include "ui_gui_mainwindow.h"

#include "model_global.h"
#include "gui_board.h"
#include "gui_newgame.h"
#include "gui_unit.h"


#include "model_serverplayer.h"
#include "gui_standardinputdialog.h"


#include <QDomDocument>
#include <QDomElement>
#include <QDebug>

#include <QFileDialog>
#include <QMessageBox>

#include <QStandardItemModel>
#include <QStandardItem>

#include <QTimer>


const QString PLAYER_STYLE_CURRENT = "color: rgb(0, 220, 0);";
const QString PLAYER_STYLE_NOT_CURRENT = "color: black;";



const QString PLAYER_MESSAGE_WIN = QString::fromUtf8("Bílý vyhrál!");
const QString PLAYER_MESSAGE_LOOSE = QString::fromUtf8("Černý vyhrál!");



MainWindow::MainWindow()
	: ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	setWindowTitle(APP_NAME);

	connect(this->ui->actionKonec, SIGNAL(triggered()), qApp, SLOT(quit()));
	connect(this->ui->actionNovaHra, SIGNAL(triggered()), this, SLOT(buttonNewGame()));
	connect(this->ui->actionUlozitHru, SIGNAL(triggered()), this, SLOT(actionSaveGame()));
	connect(this->ui->buttonNewGame, SIGNAL(clicked()), this, SLOT(buttonNewGame()));
	connect(this->ui->actioPrehravaciMod, SIGNAL(triggered()), this, SLOT(actionStartReplayMode()));
	connect(ui->actionNapoveda, SIGNAL(triggered()), this, SLOT(actionShowHints()));
	connect(ui->actionOtevritHru, SIGNAL(triggered()), this, SLOT(actionLoadGame()));



	connect(ui->buttonBegin, SIGNAL(clicked()), this, SLOT(buttonToBegin()));
	connect(ui->buttonEnd, SIGNAL(clicked()), this, SLOT(buttonToEnd()));
	connect(ui->buttonNext, SIGNAL(clicked()), this, SLOT(buttonNextMove()));
	connect(ui->buttonPrevious, SIGNAL(clicked()), this, SLOT(buttonPrevMove()));
	connect(ui->buttonPause, SIGNAL(clicked()), this, SLOT(buttonPause()));
	connect(ui->buttonPlayNext, SIGNAL(clicked()), this, SLOT(buttonPlayNext()));
	connect(ui->buttonPlayPrev, SIGNAL(clicked()), this, SLOT(buttonPlayPrev()));


	connect(ui->buttonOpenXML, SIGNAL(clicked()), this, SLOT(buttonOpenXML()));
	connect(ui->buttonEnterStandardFormat, SIGNAL(clicked()), this, SLOT(buttonEnterStandardFormat()));

	connect(ui->replaySlider, SIGNAL(sliderMoved(int)), this, SLOT(updateTimerInterval(int)));

	connect(ui->tableView, SIGNAL(clicked(QModelIndex)), this, SLOT(tableViewClickedIndex(QModelIndex)));


	ui->actionUlozitHru->setDisabled(true);
	ui->actionUlozitHruJako->setDisabled(true);
	ui->actionNapoveda->setDisabled(true);
	ui->actioPrehravaciMod->setDisabled(true);


	ui->buttonBegin->hide();
	ui->buttonEnd->hide();
	ui->buttonNext->hide();
	ui->buttonPrevious->hide();
	ui->buttonPlayNext->hide();
	ui->buttonPlayPrev->hide();
	ui->buttonPause->hide();

	ui->replaySlider->hide();

	ui->labelWaiting->hide();


	ui->localPlayerNameLabel->hide();
	ui->secondPlayerNameLabel->hide();

	_gboard = NULL;
	_board = NULL;
	_local = _second = NULL;

	_coolToMakeMove = false;
	_endOfGame = false;


	_historyModel = new QStandardItemModel;
	ui->tableView->setModel(_historyModel);
	ui->tableView->setEditTriggers(QTableView::NoEditTriggers);
	ui->tableView->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	ui->tableView->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);

	connect(&_replayTimer, SIGNAL(timeout()), this, SLOT(playMovesTo()));
}


void MainWindow::initBoard(Player * local, Player * second, Moves moves)
{
	ui->buttonNewGame->hide();
	ui->buttonNewGame->deleteLater();
	ui->buttonOpenXML->hide();
	ui->buttonEnterStandardFormat->hide();


	_local = local;
	_second = second;

	_movesToApply = moves;

	_local->setController(this);
	_second->setController(this);


	connect(_local, SIGNAL(makesMove(Player*,Coordinates,Coordinates)), this, SLOT(playerMakesMove(Player*,Coordinates,Coordinates)));
	connect(_second, SIGNAL(makesMove(Player*,Coordinates,Coordinates)), this, SLOT(playerMakesMove(Player*,Coordinates,Coordinates)));

	connect(_second, SIGNAL(connectionRefused()), this, SLOT(playerConnectionError()));
	connect(_second, SIGNAL(connectionEstablished()), this, SLOT(playerConnectionEstablished()));
	connect(_second, SIGNAL(connectionEnded()), this, SLOT(playerConnectionEnded()));
	connect(_second, SIGNAL(initBoardSetupRecieved(Moves)), this, SLOT(initBoardSetupRecieved(Moves)));
	connect(_second, SIGNAL(informationChanged()), this, SLOT(updateInformations()));


	ui->actioPrehravaciMod->setDisabled(false);



	_secondColor = new GUnit(new Unit(_second->color(), Unit::Stone), ui->playersWidget);
	_secondColor->setGeometry(QRect(7, 10, 40, 40));
	_secondColor->show();

	_localColor = new GUnit(new Unit(_local->color(), Unit::Stone), ui->playersWidget);
	_localColor->setGeometry(QRect(7, 50, 40, 40));
	_localColor->show();


	ui->localPlayerNameLabel->show();
	ui->secondPlayerNameLabel->show();
	ui->labelWaiting->show();


	updateInformations();

	_local->connectIfNeeded();
	_second->connectIfNeeded();
}

void MainWindow::createBoard()
{
	if ( ! _gboard )
	{
		_board = new Board;

		if ( ! _movesToApply.empty() )
		{
			if ( ! _board->loadGame(_movesToApply) )
			{
				QMessageBox::warning(this, QString::fromUtf8("Chybný vstupní soubor / seznam tahů."), QString::fromUtf8("Chybný vstupní soubor / seznam tahů. Neplatné tahy."));
				delete _board;
				_board = NULL;
				return;
			}
		}

		_gboard = new GBoard(*_board, this);

		ui->gridLayout->addWidget(_gboard, 0, 0, 2, 1);

		_gboard->show();

		actionShowHints();

		updateCurrentPlayer();


		ui->actionUlozitHru->setDisabled(false);
		ui->actionUlozitHruJako->setDisabled(false);
		ui->actionNapoveda->setDisabled(false);

		ui->labelWaiting->hide();


		updateHistoryTable();
	}
}

MainWindow::~MainWindow()
{
	delete ui;
	delete _board;
	delete _gboard;

	delete _local;
	delete _second;
}



void MainWindow::buttonNewGame()
{
	GNewGame * newgame = new GNewGame(this);

	Player * local = NULL, * second = NULL;

	int ret = newgame->exec(&local, &second);

	if ( ret == QDialog::Accepted )
	{
		if ( ! _gboard and ! _second )
		{
			initBoard(local, second);
		}
		else
		{
			MainWindow *newwindow = new MainWindow;
			newwindow->initBoard(local, second);
			newwindow->show();
		}
	}
}

void MainWindow::actionSaveGame()
{
	if ( _lastSavePath.length() == 0 )
		actionSaveGameAs();
	else
		saveGame();
}

void MainWindow::actionSaveGameAs()
{
	QFileDialog dialog(this, QString::fromUtf8("Uložit hru jako ..."), "~", QString::fromUtf8("Dáma hra (*.xml)"));

	dialog.setFileMode(QFileDialog::AnyFile);
	dialog.setAcceptMode(QFileDialog::AcceptSave);
	dialog.setDefaultSuffix("xml");

	int ret = dialog.exec();

	if ( ret == QFileDialog::Accepted )
	{
		QString filePath = dialog.selectedFiles().at(0);

		_lastSavePath = filePath;

		saveGame();
	}
}

void MainWindow::saveGame()
{
	QDomDocument doc;
	QDomProcessingInstruction header = doc.createProcessingInstruction( "xml", "version=\"1.0\" encoding=\"UTF-8\"" );
	doc.appendChild(header);

	QDomElement root = doc.createElement("game");

	QDomElement players = doc.createElement("players");


	QDomElement local = _local->toXML(doc);
	local.setAttribute("place", "local");
	players.appendChild(local);

	local = _second->toXML(doc);
	local.setAttribute("place", "second");
	players.appendChild(local);


	root.appendChild(players);

	root.appendChild(_board->getLog().toXML(doc));

	doc.appendChild(root);

	QFile output(_lastSavePath);

	if ( output.open(QFile::WriteOnly) )
	{
		QTextStream stream(&output);
		doc.save(stream, 4);

		output.close();
	}
}

void MainWindow::actionLoadGame()
{
	QFileDialog dialog(this, QString::fromUtf8("Otevří hru ..."), ".", QString::fromUtf8("Dáma hra (*.xml)"));

	dialog.setFileMode(QFileDialog::AnyFile);
	dialog.setDefaultSuffix("xml");

	int ret = dialog.exec();


	if ( ret == QFileDialog::Accepted )
	{
		QString filePath = dialog.selectedFiles().at(0);

		QFile file(filePath);

		file.open(QFile::ReadOnly);

		QDomDocument doc;
		doc.setContent(&file);


		QDomElement root = doc.documentElement();

		if ( root.tagName() == "game" )
		{

			Player * localPlayer, * secondPlayer;

			QDomElement players = root.firstChildElement("players");

			QDomElement local = players.firstChildElement("player");
			if ( local.attribute("place") == "local")
				localPlayer = Player::fromXML(local);

			local = local.nextSibling().toElement();

			if ( local.attribute("place") == "second")
				secondPlayer = Player::fromXML(local);

			QDomElement movesXML = root.firstChildElement("moves");

			Moves moves = Moves::fromXML(movesXML);

			if ( ! _gboard and ! _second )
			{
				initBoard(localPlayer, secondPlayer, moves);
			}
			else
			{
				MainWindow *newwindow = new MainWindow;
				newwindow->initBoard(localPlayer, secondPlayer, moves);
				newwindow->show();
			}
		}
	}
}








void MainWindow::boardWillMakeMove(Coordinates from, Coordinates to, bool &validity)
{
	if ( ! _coolToMakeMove )
	{
		validity = false;
		return;
	}


	Coordinates toRemove;
	Board::MoveError error = _board->makeMove(from, to, toRemove);

	validity = (error != Board::MOVE_ERROR);

	if ( validity )
	{
		if ( ! toRemove.isNull() )
			_gboard->removeUnit(toRemove);

		if ( error == Board::MOVE_WIN )
		{
			showWinMessage();
			_endOfGame = true;
			return;
		}

		if ( error == Board::MOVE_CHANGE )
			_gboard->updateUnit(to, _board->tile(to)->unit());


		_local->moveWasPerformed(from, to);
		_second->moveWasPerformed(from, to);

		updateHistoryTable();
		updateCurrentPlayer();
	}
}

void MainWindow::playerMakesMove(Player * player, Coordinates from, Coordinates to)
{
	if ( ! _coolToMakeMove )
		return;

	Coordinates toRemove;
	Board::MoveError error = _board->makeMove(from, to, toRemove);

	bool validity = (error != Board::MOVE_ERROR);

	if ( validity )
	{
		_gboard->makeMove(from, to);
		_gboard->updateHighlights();

		if ( ! toRemove.isNull() )
			_gboard->removeUnit(toRemove);

		if ( error == Board::MOVE_WIN )
		{
			if ( _second->isNetwork() )
				_second->moveWasPerformed(from, to);

			showWinMessage();
			_endOfGame = true;
			return;
		}

		if ( error == Board::MOVE_CHANGE )
			_gboard->updateUnit(to, _board->tile(to)->unit());

		if ( player == _local )
			_second->moveWasPerformed(from, to);
		else if ( player == _second )
			_local->moveWasPerformed(from, to);


		updateHistoryTable();
		updateCurrentPlayer();
	}
}

void setLabelStyleForPlayer(Color currentColor, Player * player, QLabel * label)
{
	QFont font = label->font();

	if ( currentColor == player->color() )
	{
		font.setBold(true);
		label->setStyleSheet(PLAYER_STYLE_CURRENT);
	}
	else
	{
		font.setBold(false);
		label->setStyleSheet(PLAYER_STYLE_NOT_CURRENT);
	}

	label->setFont(font);
}

void MainWindow::updateCurrentPlayer()
{
	if ( _board )
	{
		Color color = _board->colorOnTheMove();

		setLabelStyleForPlayer(color, _local, ui->localPlayerNameLabel);
		setLabelStyleForPlayer(color, _second, ui->secondPlayerNameLabel);
	}
}

void MainWindow::updateHistoryTable()
{
	_historyModel->clear();

	Moves moves = _board->getLog();

	for ( size_t i = 0; i < moves.size(); i++ )
	{
		QString string;

		string.append(QString("%1. ").arg((i/2)+1));

		Move * move = moves.at(i);

		string.append(move->toString());

		i++;
		if ( moves.size() > i )
		{
			move = moves.at(i);
			string.append(" " + move->toStandardFormat());
		}


		QStandardItem * item = new QStandardItem;
		item->setText(string);
		_historyModel->appendRow(item);
	}

	ui->tableView->resizeColumnsToContents();
	ui->tableView->update();
	ui->tableView->scrollToBottom();
}

void MainWindow::actionShowHints()
{
	if ( _gboard )
	{
		_gboard->setShowHints(ui->actionNapoveda->isChecked());
	}
}



void MainWindow::showWinMessage()
{
	const QString * message = NULL;

	if ( _local->color() == _board->colorOnTheMove() )
		message = &PLAYER_MESSAGE_WIN;
	else
		message = &PLAYER_MESSAGE_LOOSE;

	QMessageBox::information(this, *message, *message, QMessageBox::Ok);
}



void MainWindow::initBoardSetupRecieved(Moves moves)
{
	_local->setColor(inverseColor(_second->color()));

	_movesToApply = moves;
	createBoard();

	updateInformations();
	updateCurrentPlayer();
}
void MainWindow::playerConnectionError()
{
	std::cerr << "connection error\n";

	QMessageBox::warning(this, QString::fromUtf8("Chyba při spojení"), QString::fromUtf8("Nastala chyba, pokuste se o vytvoření hry znovu"));
}
void MainWindow::playerConnectionEstablished()
{
	if ( _local->connected() and _second->connected() )
	{
		std::cerr << "connected " + _local->name().toStdString() + "\n";

		if ( _second->type() == Player::NetworkServer )
		{
			_second->initBoardSetup(_movesToApply);
		}

		_second->nameOfOtherPlayerChanged(_local->name());


		_coolToMakeMove = true;


		if ( _second->type() != Player::NetworkClient )
		{
			createBoard();
		}


		Player * white = whitePlayer();
		if ( white->type() == Player::Computer )
			white->moveWasPerformed(Coordinates(), Coordinates());
	}
}
void MainWindow::playerConnectionEnded()
{
	std::cerr << "connection ended\n";

	if ( ! _endOfGame )
		showWinMessage();

	_endOfGame = true;
	_coolToMakeMove = false;
}





void MainWindow::closeEvent(QCloseEvent *)
{
	if ( _second )
		_second->end();
}




void MainWindow::updateInformations()
{
	setWindowTitle(QString(APP_NAME) + ": " + _local->name() + " vs " + _second->name());

	ui->secondPlayerNameLabel->setText(_second->prettyName());
	ui->localPlayerNameLabel->setText(_local->prettyName());

	_localColor->setColor(_local->color());
	_secondColor->setColor(_second->color());
}

Player * MainWindow::playerWithColor(Color color)
{
	if ( _local and _local->color() == color )
		return _local;
	else
		return _second;
}

Player * MainWindow::whitePlayer()
{
	return playerWithColor(ColorWhite);
}

Player * MainWindow::blackPlayer()
{
	return playerWithColor(ColorBlack);
}



Tiles MainWindow::tilesWithPossibleMove()
{
	Tiles tiles;

	if ( _board and _coolToMakeMove and ! _endOfGame )
	{
		tiles = _board->tilesWithPossibleMove();
	}

	return tiles;
}


Tiles MainWindow::tilesWhereCanMove(Coordinates from)
{
	Tiles tiles;

	const Tile * tile = _board->tile(from);

	if ( (tile->isOccupied() and tile->unit()->color() != _board->colorOnTheMove()) or
		 playerWithColor(_board->colorOnTheMove())->type() != Player::Human or
		 ui->actioPrehravaciMod->isChecked() )
	{
		return tiles;
	}


	if ( _board and _coolToMakeMove and ! _endOfGame )
	{
		tiles = _board->tilesWhereCanMove(from);
	}

	return tiles;
}

Moves MainWindow::possibleMovesWithUnitColor(Color color)
{
	if ( _board and _coolToMakeMove and ! _endOfGame )
	{
		return _board->getHints(color);
	}

	return Moves();
}





void MainWindow::buttonOpenXML()
{
	actionLoadGame();
}

void MainWindow::buttonEnterStandardFormat()
{
	StandardInputDialog st;
	QString * string = st.execDialog();

	if ( string )
	{
		Moves moves = Moves::fromStandardFormat(string->toLocal8Bit());

		GNewGame * newgame = new GNewGame(this);

		Player * local = NULL, * second = NULL;

		int ret = newgame->exec(&local, &second);

		if ( ret == QDialog::Accepted )
		{
			if ( ! _gboard and ! _second )
			{
				initBoard(local, second, moves);
			}
			else
			{
				MainWindow *newwindow = new MainWindow;
				newwindow->initBoard(local, second, moves);
				newwindow->show();
			}
		}

	}
}





void MainWindow::actionStartReplayMode()
{
	ui->buttonBegin->show();
	ui->buttonEnd->show();
	ui->buttonNext->show();
	ui->buttonPrevious->show();
	ui->buttonPlayNext->show();
	ui->buttonPlayPrev->show();
	ui->buttonPause->show();

	ui->replaySlider->show();

	// zabránění pohybů s jednotkama
	_coolToMakeMove = false;


	_second->end();

	if ( ! _gboard )
		createBoard();

	_gboard->updateHighlights();

	ui->tableView->selectRow(_board->logIndex()/2-1);
}

void MainWindow::updateTimerInterval(int msec)
{
	_replayTimer.setInterval(msec);
}


Move * MainWindow::_nextMove()
{
	Move * move = _board->nextMove();
	if ( move )
	{
		_gboard->makeMove(move->from(), move->to());

		if ( ! move->taken().isNull() )
			_gboard->removeUnit(move->taken());

		if ( move->unitChanged() )
			_gboard->updateUnit(move->to(), move->activeUnit());

		ui->tableView->clearSelection();
		ui->tableView->selectRow(_board->logIndex()/2-1);

	}
	return move;
}

Move * MainWindow::_prevMove()
{
	Move * move = _board->prevMove();
	if ( move )
	{
		_gboard->makeMove(move->to(), move->from());

		if ( ! move->taken().isNull() )
			_gboard->addUnit(move->taken(), move->takenUnit());

		if ( move->unitChanged() )
			_gboard->updateUnit(move->from(), move->activeUnit());

		ui->tableView->clearSelection();
		ui->tableView->selectRow(_board->logIndex()/2-1);
	}

	return move;
}

void MainWindow::_moveToBegin()
{
	_moveToIndex = 0;
	playMovesTo();
}

void MainWindow::_moveToEnd()
{
	_moveToIndex = _board->getLog().size();
	playMovesTo();
}

void MainWindow::buttonNextMove()
{
	_replayTimer.stop();
	_nextMove();
}

void MainWindow::buttonPrevMove()
{
	_replayTimer.stop();
	_prevMove();
}


void MainWindow::buttonToBegin()
{
	updateTimerInterval(40);
	_moveToBegin();
}

void MainWindow::buttonToEnd()
{
	updateTimerInterval(40);
	_moveToEnd();
}




void MainWindow::buttonPlayNext()
{
	updateTimerInterval(ui->replaySlider->value());
	_moveToEnd();
}

void MainWindow::buttonPlayPrev()
{
	updateTimerInterval(ui->replaySlider->value());
	_moveToBegin();
}

void MainWindow::buttonPause()
{
	_replayTimer.stop();
}



void MainWindow::tableViewClickedIndex(QModelIndex index)
{
	if ( ui->actioPrehravaciMod->isChecked() )
	{
		updateTimerInterval(40);
		_moveToIndex = (index.row() + 1) * 2;
		_replayTimer.start();
		playMovesTo();
	}
}

void MainWindow::playMovesTo()
{
	if ( ! _replayTimer.isActive() )
		_replayTimer.start();


	if ( _moveToIndex > _board->logIndex() )
		_nextMove();
	else if ( _moveToIndex < _board->logIndex() )
		_prevMove();
	else
		_replayTimer.stop();
}
