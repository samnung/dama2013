/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Vojtěch Dvořáček (xdvora0y)
 *
 * Popis: třída popisující CLI rozhraní
 *
 * encoding: utf-8
 ******************************************************************************/

#ifndef CLI_MAIN_H
#define CLI_MAIN_H

#endif // CLI_MAIN_H

#include "model_board.h"
#include "model_global.h"
#include <string>
#include <iostream>
#include <fstream>


/**
* trida pro prikazovou radku
* ridi CLI hru a vola metody desky
**/


class CLI
{

private:
    std::string _input, _playerWhite, _playerBlack, _moveCoordsStr, _PS;
    Coordinates _moveFrom, _moveTo;

    enum Status {PLAYING, DEFAULT};
    Status _state;

    bool getCoords();
    std::string nameOnTheMove(Board &board, bool logic = true);
    std::string nameNotOnTheMove(Board & board);
public:
    CLI(): _input(), _playerWhite(),_playerBlack(), _moveCoordsStr(), _moveFrom(),_moveTo()
    {_PS = "dama 2013"; _state = Status::DEFAULT; }

	enum Command {C_ERR = -1, C_HELP, C_NEW_GAME, C_MOVE, C_SURR, C_QUIT, C_SHOW_HIST, C_SHOW_BOARD};


    bool isIn(std::string pattern);
    Command getCommandType();
    int runCli();
    void printHelp();
    bool execCommand(Command ret, Board &board);
};





