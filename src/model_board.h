/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05), Vojtěch Dvořáček(xdvora0y)
 *
 * Popis: Třída představující hrací desku
 *
 * encoding: utf-8
 ******************************************************************************/

#ifndef MODEL_BOARD_H
#define MODEL_BOARD_H


#include "model_global.h"
#include "model_tile.h"
#include "model_move.h"


class Moves;

/**
 * @class Board
 *
 * Třída představující hrací desku.
 *
 * Kontroluje validitu tahů, ukládá seznam použitých tahu (Move).
 */
class Board
{
	Tiles _tiles;
	Moves _moveLog;
	size_t _logIndex;

public:

	/**
	 * Error flagy, které vrací metoda makeMove()
	 */
	enum MoveError {
		MOVE_ERROR = -1,	///< vstupní tah je neplatný
		MOVE_OK,			///< vstupní tah je platný
		MOVE_CHANGE,		///< vstupní tah je platný, ale při přesouvání nastala změna figurky
		MOVE_WIN			///< vstupní tah je platný, ale hráč který byl na tahu vyhrál
	};

	/**
	 * Konstruktor desky
	 */
	Board();

	/**
	 * Validuje tah
	 * @param from	počáteční souřadnice
	 * @param to	cílová souřadnice
	 * @return true == vstupní tah je validní
	 */
	bool canMakeMove(Coordinates from, Coordinates to);

	/**
	 * Provede tah
	 *
	 * Napřed provádí validaci tahu
	 *
	 * @param from		počáteční souřadnice
	 * @param to		cílová souřadnice
	 * @param removed	pokud není Null, figurka na souřadnicích zmizela, viz Coordinates::isNull()
	 * @return	validitu tahu, viz #MoveError
	 */
	MoveError makeMove(Coordinates from, Coordinates to, Coordinates &removed);

	/**
	 * Získání seznamu políček, na které se může jít ze vstupní souřadnice
	 *
	 * @param from			počáteční pozice
	 * @param hasPriority	souřadnice figurky, která má přednost
	 * @return	pokud není hasPriority Null, tak seznam políček, na která se dá pohnout z počáteční pozice,
	 *			jinak seznam políček, na která se dá jít ze souřadnice hasPriority
	 */
	Tiles getHint(Coordinates from, Coordinates & hasPriority);

	/**
	 * Získání seznamu tahů, které se dají provést s danou barvou figurky
	 *
	 * @param forColor barva figurky, na kterou chceme získat seznam tahů
	 * @return seznam tahů
	 */
	Moves getHints(Color forColor);

	/**
	 * Zjištění barvy hráče, která je na tahu
	 *
	 * @return
	 */
	Color colorOnTheMove() const { return _onTheMove; }


	/**
	 * Zjištění seznamu políček, ze který se dá provést tah na jíné políčko
	 *
	 * @return seznam políček s možným tahem
	 */
	Tiles tilesWithPossibleMove();

	/**
	 * Zjištění seznamu políček, na která se dá jít ze vstupní souřadnice
	 *
	 * @param from		počáteční souřadnice
	 * @return seznam volných políček, na která se dá jít ze vstupní souřadnice
	 */
	Tiles tilesWhereCanMove(Coordinates from);



	/**
	 * Získání seznamů provedených tahů
	 *
	 * @return seznam tahů
	 */
	Moves & getLog() { return _moveLog; }

	/**
	 * Získání aktuální pozice v historii provedených tahů
	 *
	 * @return aktuální index v historii
	 */
	const size_t & logIndex() const { return _logIndex; }

	/**
	 * Provedení tahů pro inicializaci desky (načtení hry ze souboru)
	 *
	 * @param movesList		seznam tahů, které se mají provést
	 * @return		validitu vstupu
	 */
	bool loadGame(Moves movesList);

	/**
	 * Získání dalšího tahu ( replay mode )
	 *
	 * @return další tah, který se provedl
	 */
	Move * nextMove();

	/**
	 * Získání předchozího tahu ( replay mode )
	 *
	 * @return předchozího tah, který se provedl
	 */
	Move * prevMove();




	/**
	 * Seznam políček, pro přístup je nutné použí funkci mapFunction()
	 *
	 * @return seznam políček
	 */
	const Tiles & tiles() const { return _tiles; }

	/**
	 * Získání ukazatele na políčko
	 *
	 * @param x x souřadnice
	 * @param y y souřadnice
	 * @return ukazatel na políčko, jinak výjimka
	 */
	const Tile * tile(Coordinate x, Coordinate y) const;

	/**
	 * Získání ukazatele na políčko
	 *
	 * @param xy souřadnice
	 * @return ukazatel na políčko, jinak výjimka
	 */
	const Tile * tile(Coordinates xy) const;


private:

	Color _onTheMove;
	void initBoard();

	Tile * _tile(Coordinate x, Coordinate y) const;
	Tile * _tile(Coordinates xy) const;

	Tiles tilesBetween(Coordinates from, Coordinates to) const;
	Units unitsBetween(Coordinates from, Coordinates to) const;

	/**
	  * overuje povinnost brani
	  * vraci vector kamenu k brani
	  * vyzaduje valici pomoci canMakeMove
	  * @param: aktivni jednotka
	 **/
	Tiles tilesToBeTaken(const Tile * activeTile) const;
	const Coordinates rotated(Color unitColor, const Coordinates toBeRotated) const;

	/**
	 * Vytvoří seznam všech políček, které jsou obsazeny a barva jednotky je podle @param unitColor.
	 * @param unitColor	barva jednotek, které požadujeme
	 * @return seznam políček obsahující jednotku s danou barvou
	 */
	Tiles tilesByUnitColor(Color unitColor);

	/**
	 * Kontroluje prednost brani pro vsechny hrace stejne barvy
	 * @param barva hrace na tahu
	 * @param souradni
	 */
	Tile * hasTakingPriority(const Coordinates from);

	friend std::ostream & operator << (std::ostream & os, const Board & board);
};

extern size_t mapFunction(Coordinate x, Coordinate y);
extern size_t mapFunction(Coordinates xy);

extern bool isItBlackCoordinate(Coordinate x, Coordinate y);



#endif
