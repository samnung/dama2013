/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis:
 *
 * encoding: utf-8
 ******************************************************************************/

#include "gui_standardinputdialog.h"
#include "ui_gui_standardinputdialog.h"

StandardInputDialog::StandardInputDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::StandardInputDialog)
{
	ui->setupUi(this);
}

StandardInputDialog::~StandardInputDialog()
{
	delete ui;
}


QString * StandardInputDialog::execDialog()
{
	if ( QDialog::exec() == QDialog::Accepted )
	{
		return new QString(ui->textEdit->toPlainText());
	}

	return NULL;
}
