/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: "protokol" pro controller
 *
 * encoding: utf-8
 ******************************************************************************/

#ifndef MODEL_BSTRACTCONTROLLER_H
#define MODEL_BSTRACTCONTROLLER_H

class Coordinates;
class Board;
class Moves;

#include "model_tile.h"

/**
 * @class AbstractController
 *
 * Protokol, který zajišťuje příjem notifikací od desky a zpřístupnění některých funkcí z desky pro hráče
 *
 * Třída, která se měla použít pro CLI controller, z důvodu času se z toho sešlo
 */
class AbstractController
{
public:
	/**
	 * Metoda pro notifikaci od desky, že hráč provedl tah
	 *
	 * @param from		počáteční pozice
	 * @param to		cílová pozice
	 * @param validity	pro vrácení validity pohybu
	 */
	virtual void boardWillMakeMove(Coordinates from, Coordinates to, bool & validity) = 0;

	/**
	 * Získání políček, ze kterých můžu táhnout.
	 * @return seznam políček
	 */
	virtual Tiles tilesWithPossibleMove() = 0;

	/**
	 * Získání políček, do kterých můžu provést tah, z dané souřadnice.
	 * @param from	vstupní souřadnice, na kterou hledám tah
	 * @return		seznam políček, na který můžu táhnout
	 */
	virtual Tiles tilesWhereCanMove(Coordinates from) = 0;

	/**
	 * Získání seznamu možných tahu pro danou barvu jednotky / hráče.
	 * @param color		barva, pro kterou chceme možné tahy
	 * @return			seznam možných tahů
	 */
	virtual Moves possibleMovesWithUnitColor(Color color) = 0;
};

#endif // MODEL_BSTRACTCONTROLLER_H
