/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: třída pro popis GUI hrací jednotky
 *
 * encoding: utf-8
 ******************************************************************************/

#ifndef GUI_UNIT_H
#define GUI_UNIT_H

#include <QWidget>
#include "model_unit.h"


/**
 * @class GUnit
 *
 * Třída reprezentující figurka na grafické desce.
 *
 * Zajišťuje vykreslování figruky
 */
class GUnit : public QWidget, public Unit
{
	Q_OBJECT

public:
	/**
	 * Konstruktor figurky
	 *
	 * @param unit		modelová figurka, podle které se nastaví informace (barva, typ)
	 * @param parent	QWidget parent
	 */
	explicit GUnit(const Unit * unit, QWidget *parent = 0);


	/**
	 * Konstruktor pro vytvoření jednotky z řetězce.
	 *
	 * Používá se při přesouvání jednotky pomocí Drag & Drop
	 *
	 * @param string	vstupní řetězec
	 * @param parent	stejné jako GUnit::GUnit
	 * @return ukazatel na vytvořený objekt
	 */
	static GUnit * fromString(const QString & string, QWidget * parent = 0);

	/**
	 * Převod jednotky na řetězec, viz fromString()
	 *
	 * @return vrací řetězec, na základě kterého se dokáže znovu vytvořit figurka se stejnými vlastnostmi
	 */
	QString toString() const;

	/**
	 * Převod figurky na obrázek, který se dá použít pro Drag & Drop.
	 *
	 * V podstatě jde o vykreslení QWidgetu do obrázku v QPixmap.
	 *
	 * @return obrázek, který se použije pro Drag & Drop
	 */
	QPixmap toPixmap();

	/**
	 * Mime data identifikátor pro Drag & Drop
	 * @return	řetězec, podle kterého se dá poznat, zda se vkládá na políčko figurka nebo něco jiného
	 */
	static const QString & mimeData();


	/**
	 * Nastavení barvy figurky
	 *
	 * Nastaví svou barvu a vykreslí se
	 *
	 * @param color		barva, na kterou se má změnit
	 */
	void setColor(Color color) { _color = color; update(); }

	/**
	 * Změna typu figurky
	 *
	 * Tato metoda je důležitá při transformaci figurky např. z kamene na dámu
	 *
	 * @param type
	 */
	void setType(Type type) { _type = type; update(); }

	/**
	 * Obecná aktualizace figurky na základě grafické figurky
	 *
	 * @param unit	modelová figurka, na základě které chceme aktualizovat grafickou figurku
	 */
	void updateBy(Unit * unit);



protected:
	// paint
	void paintEvent(QPaintEvent *e);


};

#endif // GUI_UNIT_H
