/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: třída pro vytvoření dialogového okna pro načtení řetězce
 *
 * encoding: utf-8
 ******************************************************************************/

#ifndef GUI_STANDARDINPUTDIALOG_H
#define GUI_STANDARDINPUTDIALOG_H

#include <QDialog>

namespace Ui {
class StandardInputDialog;
}

/**
 * Dialogové okno pro vložení standardní notace, dá se použt i jako standardní vstup
 *
 * Vrací ukazatel na vložený řetězec
 */
class StandardInputDialog : public QDialog
{
	Q_OBJECT

public:
	/**
	 * Konstruktor
	 * @param parent
	 */
	explicit StandardInputDialog(QWidget *parent = 0);
	~StandardInputDialog();

	/**
	 * Vytvoří modální dialogové okno
	 *
	 * @return	ukazatel na řetězec - pokud uživatel klikl na Ok
	 *			NULL - pokud klikl na Zrušit / Cancel
	 */
	QString * execDialog();

private:
	Ui::StandardInputDialog *ui;
};

#endif // GUI_STANDARDINPUTDIALOG_H
