/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: třída představující abstraktního hráče
 *
 * encoding: utf-8
 ******************************************************************************/

#ifndef MODEL_PLAYER_H
#define MODEL_PLAYER_H

#include "model_global.h"

#include <QObject>

#include "model_move.h"



class AbstractController;


/**
 * Třída pro normálního hráče, který definuje zprávy, které můžou zasílat hráčí.
 *
 * Většina zpráv je pro základního hráče nedůležitá, nevytváří žádné odpovědi, ani nijak jinak nereaguje.
 * Zprávy jsou určeny pro podtřídy (počítač, síťový hráč), kteří dělají něco navíc (komunikace po síti nebo vytváření odpovědi na tah).
 */
class Player : public QObject
{
	Q_OBJECT

public:

	enum Type {
		Human,
		Computer,
		NetworkClient,
		NetworkServer
	};

	static QString typeToString(Type type);
	static Type stringToType(const QString & str);

protected:

	QString _name;
	Color _color;

	AbstractController * _controller;


public:
	/**
	 * Konstruktor normálního hráče (člověk)
	 * @param name			jméno hráče
	 * @param controller	ukazazel kontroller
	 * @param parent		QObject parent
	 */
	explicit Player(const QString & name, AbstractController * controller = NULL, QObject * parent = NULL);


	void setName(const QString & name);
	void setColor(const Color & color);

	const QString & name() const { return _name; }
	const Color & color() const { return _color; }


	/**
	 * Metoda pro získání jména, které se použije pro výpis
	 * @return	jméno hráče + informace jakého typu je hráč
	 */
	virtual QString prettyName() const { return _name; }


	void setController(AbstractController * controller) { _controller = controller; }


	/**
	 * Metoda pro zjištění typu hráče
	 * @return typ hráče
	 */
	virtual Type type() const { return Human; }

	bool isNetwork() const { return (type() == NetworkClient or type() == NetworkServer); }


	/**
	 * Metoda pro oznamování, že byl proveden tah
	 *
	 * Hráč na tuto zprávu nijak nereaguje, používá se pro podtřídy (počítač, síť)
	 *
	 * @param from		počáteční souřadnice tahu
	 * @param to		koncová souřadnice tahu
	 */
	virtual void moveWasPerformed(Coordinates from, Coordinates to);


	/**
	 * Metoda pro vytvoření spojení
	 *
	 * Ihned vyvolá signál connectionEstablished()
	 *
	 * Používá se pro síťové hráče
	 */
	virtual void connectIfNeeded();

	/**
	 * Metoda pro zjištění, zda je hráč připojen
	 * @return stav připojení
	 */
	virtual bool connected();


	/**
	 * Oznámení, že úvodní stav bude jiný, na základě seznamu pohybů
	 *
	 * Hráč na tuto zprávu nijak nereaguje, používá se pro podtřídy (počítač, síť)
	 *
	 * @param moves		seznam pohybů, které je nutné vykonat pro synchronizaci desek
	 */
	virtual void initBoardSetup(Moves & moves);


	/**
	 * Oznámení konce hry
	 *
	 * Hráč na tuto zprávu nijak nereaguje, používá se pro podtřídy (počítač, síť)
	 */
	virtual void end();


	/**
	 * Oznámení o změně jména
	 *
	 * Hráč na tuto zprávu nijak nereaguje, používá se pro podtřídy (počítač, síť)
	 */
	virtual void nameOfOtherPlayerChanged(const QString & name);




	/**
	 * Metoda pro uložení informací hráče do XML
	 * @param doc	vstupní dokument pro vytváření elementů
	 * @return vytvořený element
	 */
	virtual QDomElement toXML(QDomDocument & doc) const;


	/**
	 * Metoda pro aktualizace hráče z XML, kvůli polymorfismu
	 * @param el
	 */
	virtual void updateWithXML(const QDomElement & el);


	/**
	 * Funkce pro vytvoření hráče z XML elementu
	 *
	 * !!! Pokud hru ukládá klient, vytvoří se jako server (jinak by nemohla hra vůbec začít) !!!
	 *
	 * @param element vstupní element se značkou <player>
	 * @return vytvořený hráč
	 */
	static Player * fromXML(const QDomElement & element);


	/**
	 * Funkce pro vytvoření hráče, na základě @param type, může se vytvořit podtřída (počítač, síť)
	 * @param type	typ hráče, který chceme vytvořit
	 * @param name	jméno hráče
	 * @return vytvořený hráč
	 */
	static Player * factoryPlayer(Type type, const QString & name);


signals:

	/**
	 * Signál o tom, že hráč provedl tah
	 *
	 * Hráč tento signál nevysílá, používá se pro podtřídy (počítač, síť)
	 * @param player	který hráč
	 * @param from		počáteční souřadnice tahu
	 * @param to		koncová souřadnice tahu
	 */
	void makesMove(Player * player, Coordinates from, Coordinates to);

	/**
	 * Hráč obdržel informace o počátečním stavu desky
	 *
	 * Hráč tento signál nevysílá, používá se pro podtřídy (síť)
	 * @param moves		seznam pohybů pro synchronizaci
	 */
	void initBoardSetupRecieved(Moves moves);

	/**
	 * Signál, který oznamuje, že hráč změnil nějakou svou informaci (jméno, barvu)
	 *
	 * Hráč tento signál nevysílá, používá se pro podtřídy (síť)
	 */
	void informationChanged();

	/**
	 * Oznámení o uskutečnění spojení
	 */
	void connectionEstablished();

	/**
	 * Oznámení o ukončení spojení
	 */
	void connectionEnded();

	/**
	 * Oznámení o chybě spojení
	 */
	void connectionRefused();

};

#endif // MODEL_PLAYER_H
