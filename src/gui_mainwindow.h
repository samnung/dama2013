/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: třída popisující hlavní okno aplikace
 *
 * encoding: utf-8
 ******************************************************************************/

#ifndef MAINDWINDOW_H
#define MAINDWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QModelIndex>

#include "model_board.h"
#include "model_player.h"
#include "gui_board.h"

#include "model_abstractcontroller.h"

class Player;
class QStandardItemModel;

namespace Ui {
class MainWindow;
}

/**
 * @class MainWindow
 *
 * Zaobaluje zobrazení hlavního okna a stará se o chování celé aplikace, chová se jako Controller z MVC.
 *
 * Propojuje:
 *		- Model:
 *			- Board jako desku
 *			- Player jako dvě instance hráčů
 *		- View:
 *			- GBoard jako grafickou desku
 */
class MainWindow : public QMainWindow, public AbstractController
{
	Q_OBJECT


public:
	/**
	 * Kontruktor hlavního okna
	 *
	 * Nastaví okno do defaultního stavu
	 */
	MainWindow();

	~MainWindow();


	/**
	 * Signál se vyvolá, když uživatel změní pozici figurky
	 * @param from		počáteční pozice
	 * @param to		cílová pozice
	 * @param validity	pro vrácení validity pohybu
	 */
	void boardWillMakeMove(Coordinates from, Coordinates to, bool & validity) override;


	/**
	 * Získání políček, ze kterých můžu táhnout.
	 * @return seznam políček
	 */
	virtual Tiles tilesWithPossibleMove() override;

	/**
	 * Získání políček, do kterých můžu provést tah, z dané souřadnice.
	 * @param from	vstupní souřadnice, na kterou hledám tah
	 * @return		seznam políček, na který můžu táhnout
	 */
	virtual Tiles tilesWhereCanMove(Coordinates from) override;

	/**
	 * Získání seznamu možných tahu pro danou barvu jednotky / hráče.
	 * @param color		barva, pro kterou chceme možné tahy
	 * @return			seznam možných tahů
	 */
	virtual Moves possibleMovesWithUnitColor(Color color) override;


private:
	// UI
	Ui::MainWindow *ui;

	// event
	void closeEvent(QCloseEvent *);

	// game
	void initBoard(Player * local, Player * second, Moves moves = Moves());
	void createBoard();

	// aktualizace informací okolo desky
	void updateCurrentPlayer();
	void updateHistoryTable();


	// zobrazení okna při ukončení hry
	void showWinMessage();

	// uložení hry na cestu v _lastSavePosition
	void saveGame();

	// získání hráče podle barvy
	Player * playerWithColor(Color color);
	Player * whitePlayer();
	Player * blackPlayer();



	// ----------- model -------------
	Board * _board;

	Player * _local;
	Player * _second;

	// ------------- view -------------
	GBoard *_gboard;
	GUnit * _localColor; // zobrazení barvy hráče
	GUnit * _secondColor; // zobrazení barvy hráče

	// řízení hry ( ovládání jednotek )
	bool _coolToMakeMove;
	bool _endOfGame;


	// save
	QString _lastSavePath;


	// load
	Moves _movesToApply;


	// --------- replay -----------
	QTimer _replayTimer;
	size_t _moveToIndex;
	QStandardItemModel * _historyModel;




private slots:

	// ------- player actions ----------
	void playerMakesMove(Player * player, Coordinates from, Coordinates to);
	void initBoardSetupRecieved(Moves moves);
	void playerConnectionError();
	void playerConnectionEstablished();
	void playerConnectionEnded();

	void updateInformations();

	// ----- menu actions --------
	void actionSaveGame();
	void actionSaveGameAs();
	void actionLoadGame();
	void actionStartReplayMode();
	void actionShowHints();

	// ------ init buttons -------------
	void buttonNewGame();
	void buttonOpenXML();
	void buttonEnterStandardFormat();



	// ---------- replay --------------
	Move * _nextMove();
	Move * _prevMove();

	void _moveToEnd();
	void _moveToBegin();

	void buttonPlayNext();
	void buttonPlayPrev();

	void buttonPause();

	void buttonNextMove();
	void buttonPrevMove();

	void buttonToEnd();
	void buttonToBegin();

	void updateTimerInterval(int msec);

	void tableViewClickedIndex(QModelIndex index);

	void playMovesTo();

};

#endif // MAINDWINDOW_H
