/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Vojtěch Dvořáček (xdvora0y)
 *
 * Popis: implementace metod CLI rozhraní
 *
 * encoding: utf-8
 ******************************************************************************/


#include "cli_main.h"
#include <QRegExp>



bool CLI::isIn(std::string pattern)
{
   return (_input.find(pattern) != std::string::npos);
}

/**
* parsuje vstupni prikaz
* vraci jeho enum Command ekvivalent
* nastaveuje privatni promenne
    _playerWhite, _playerBlack, _moveCoordsStr,
**/
CLI::Command CLI::getCommandType()
{
    if ( isIn("N") )  //nova hra, jmeno hrace
	{
		size_t offset1 = _input.find(" ");
		size_t offset2 = _input.find(" ", offset1 + 1);

		if( offset1 == std::string::npos or offset2 == std::string::npos)
		{
			return C_ERR;
		}
		else
		{
            _playerWhite = _input.substr(offset1 +1, offset2 -1);
            _playerBlack = _input.substr(offset2 + 1, _input.length() - 1);
		}
		return C_NEW_GAME;
	}
    else if ( isIn("M") ) //pohyb
	{
		size_t coorBegin = _input.find(" ");
		size_t coorEnd  =_input.find_last_of("123456789", _input.length() -1);

		if( coorBegin == std::string::npos or coorEnd == std::string::npos )
			return C_ERR;
		else
		{
			coorBegin += 1;
            _moveCoordsStr = _input.substr(coorBegin, coorEnd);
			return C_MOVE;
		}
	}
    //poradi!!!
    else if ( isIn("SH") ) //vypsat historii
		return C_SHOW_HIST;
    else if (isIn("help") or isIn("H") ) //napoveda
        return C_HELP;

    else if ( isIn("S") ) //vzdat hru
        return C_SURR;


    else if ( isIn("B") ) //vykreslit desku, default
		return C_SHOW_BOARD;

    else if ( isIn("Q") ) //opustit hru
		return C_QUIT;
    else if (_input == "")
        return C_SHOW_BOARD;
	else
        return C_ERR;

}
/**
* prevadi privatni _moveCoordsStr na _moveFrom a _moveTo Coordinates
* pri neuspechu vraci false
**/

bool CLI::getCoords()
{
    _moveFrom = Coordinates::fromStandardFormat(_moveCoordsStr);
//    std::cout<< "mezi " << _moveCoordsStr << std::endl;
    _moveTo = Coordinates::fromStandardFormat(_moveCoordsStr);

    if(_moveFrom.isNull() or _moveTo.isNull() )
        return false;

    return true;
}

using namespace std;

void CLI::printHelp()
{
    cout << "nova hra                           N <bily hrac> <cerny hrac>" << endl;
    cout << "pohyb                              M <odkud>-<kam>" << endl;
    cout << "vypis historii                     SH" << endl;
    cout << "ukaz desku                         B / prazdny vstup" << endl;
    cout << "tato napoveda                      H" << endl;
    cout << "vzdej hru - vitezi souper          S" << endl;
    cout << "ukoncit hru                        Q" << endl << endl;
    return;
}
/**
* vraci jmeno hrace na tahu podle barvy v desce
* @param bool, false vraci jmeno hrace, ktery neni na tahu
**/
std::string CLI::nameOnTheMove(Board & board, bool logic)
{

    if( board.colorOnTheMove() == Color::ColorWhite)
        return (logic ? _playerWhite : _playerBlack);
    else
        return (logic ? _playerBlack: _playerBlack);
}

/**
* vraci jmeno hrace, ktery neni na tahu podle barvy v desce
* vola nameOnTheMove()
**/
std::string CLI::nameNotOnTheMove(Board &board)
{
    return nameOnTheMove(board, false);
}

/**
* switch automat na provadeni prikazu
* @param: enum Command s typen operace
* @param: ref. na desku
* vraci false, pokud se ma program ukoncit
**/

bool CLI::execCommand(Command ret, Board & board)
{
    Coordinates blind;

    switch(ret)
    {
        case Command::C_NEW_GAME:
            cout << "Zacala nova hra" << endl <<  "bily: "<<_playerWhite << endl << "cerny: " << _playerBlack << endl;
            if(_state == Status::PLAYING)
                cerr << "Hra jiz bezi!(ukoncite Q)" << endl;
            else
                _state = Status::PLAYING;
            break;
        case Command::C_HELP:
            CLI::printHelp();
            break;
        case Command::C_MOVE:
            cout << _moveCoordsStr << endl;
            if( !getCoords() )
                cerr << "Spatne zadane souradnice" << endl;

            if( board.makeMove(_moveFrom, _moveTo, blind) == Board::MoveError::MOVE_ERROR )
                cerr << "Neplatny pohyb " << _moveFrom << "-> " << _moveTo <<  endl;

            if( board.makeMove(_moveFrom, _moveTo, blind) == Board::MoveError::MOVE_WIN )
                cout << nameOnTheMove(board) << " vyhral!" << endl;

            break;
        case Command::C_SURR:
            char in;
            cout << "Opravdu chce " << nameOnTheMove(board) << " vzdat hru? [y/n]:";
            cin >> in;
           if( tolower(in) == 'y' )
                cout << nameNotOnTheMove(board) << " vyhral!" << endl;
            cout << endl;
            break;
        case Command::C_QUIT: //konec
            return false;
            break;

        case Command::C_SHOW_HIST:
        {
            Moves log = board.getLog();
            unsigned count = 1;
            for (Move * it: log)
            {
                (count % 2) ? cout << "W " : cout << "B ";
                cout << count << ": "<< *it << endl;
                count++;
            }
            //historie
            break;
        }
        case Command::C_SHOW_BOARD:
            cout << endl <<  board;
            break;

        case Command::C_ERR:
            cout << "Neplatny prikaz" << endl;
            break;

    }
    return true;
}

/**
* telo cli
**/
int CLI::runCli()
{


    string answer;
    Board board;

    cout << "--------------------Vita Vas dama 2013----------------------------" << endl;
    cout << "--------------------Ovladani je nasledujici-----------------------" << endl << endl;
    printHelp();

    char inLine[100];

    while(true)
    {
        if(_state == Status::PLAYING)
        {
            if(board.colorOnTheMove() == Color::ColorWhite)
                _PS = "W " + _playerWhite;
            else
                _PS = "B " + _playerBlack;
        }

        cout << _PS << ">";
        cin.getline(inLine, 100);

        if(cin.eof())
            break;

        _input = inLine;
//        cout << _input;

        Command ret = getCommandType();
//        cout << ret << endl ;

        if( !execCommand(ret, board) ) //konec
            break;
    }

    return 0;
}

int main()
{
    CLI interface;

    interface.runCli();

    return 0;
}
