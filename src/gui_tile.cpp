/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: metody GUI pro práci s políčkem
 *
 * encoding: utf-8
 ******************************************************************************/

#include "gui_tile.h"

#include <QPaintEvent>
#include <QPainter>
#include <QMimeData>
#include <QDrag>

#include "gui_unit.h"


GTile::GTile(const Tile *tile, QWidget *parent) : QWidget(parent), Tile(*tile)
{
	_unit = NULL;
	_drag = false;
	_mouseClick = false;
	_selected = false;
	_mouseLastClick = QPoint();

	setAcceptDrops(true);

	if ( Tile::isOccupied() )
	{
		_unit = new GUnit(Tile::unit(), this);
	}
}


void GTile::resizeEvent(QResizeEvent *)
{
	updateUnitGeometry();
}

void GTile::updateUnitGeometry()
{
	if ( _unit )
		_unit->setGeometry(rect());
}

void GTile::paintEvent(QPaintEvent *e)
{
	QPainter painter;
	painter.begin(this);

	QColor _color = ( color() == ColorBlack ) ? Qt::black : Qt::white;
	painter.fillRect(e->rect(), _color);

	if ( _highlightColor.isValid() )
		painter.fillRect(e->rect(), _highlightColor);

	if ( _selected )
		painter.fillRect(e->rect(), QColor(240, 20, 20, 180));

	painter.end();
}



bool GTile::isSelected()
{
	return _selected;
}

void GTile::setSelected(bool selected)
{
	_selected = selected;
	update();
}


void GTile::setColorHighlight(QColor color)
{
	_highlightColor = color;
	update();
}



void GTile::setUnit(GUnit *unit)
{
	_unit = unit;
	_unit->setParent(this);
	_unit->show();

	updateUnitGeometry();

	update();

}
GUnit * GTile::removeUnit()
{
	GUnit * tmp = _unit;
	_unit = NULL;

	tmp->hide();

	return tmp;
}


/**********************************************************************
 *							Drag & Drop
 *********************************************************************/
void GTile::dragEnterEvent(QDragEnterEvent *e)
{
	if ( e->mimeData()->hasFormat(GUnit::mimeData()) and _unit == NULL )
	{
		e->accept();
		_drag = true;
		update();
	}
	else
	{
		e->ignore();
	}
}

void GTile::dragLeaveEvent(QDragLeaveEvent *e)
{
	_drag = false;
	update();
	e->accept();
}

void GTile::dragMoveEvent(QDragMoveEvent *e)
{
	if ( e->mimeData()->hasFormat(GUnit::mimeData()))
	{
		e->setDropAction(Qt::MoveAction);
		e->accept();
	}
	else
	{
		e->ignore();
	}

	update();
}


void GTile::mousePressEvent(QMouseEvent *e)
{
	_mouseLastClick = e->pos();
	_mouseClick = true;
}

void GTile::mouseMoveEvent(QMouseEvent *e)
{
	// ----- začíná drag akce -----
	QPoint point = e->pos() - _mouseLastClick;
	if ( _mouseClick && point.manhattanLength() > 3 && _unit )
	{
		auto unit = _unit;

		bool validity = false;
		emit unitWillPickUp(this, validity);

		if ( validity )
		{
			_unit = NULL;

			QByteArray array;
			array.append(unit->toString());

			QMimeData * data = new QMimeData;
			data->setData(GUnit::mimeData(), array);

			QDrag * drag = new QDrag(this);
			drag->setMimeData(data);
			drag->setHotSpot(e->pos() - rect().topLeft());
			drag->setPixmap(unit->toPixmap());

			unit->hide();

			emit unitDidPickUp(this);

			if ( (drag->exec(Qt::MoveAction) != Qt::MoveAction) )
			{
				_unit = unit;
				unit->show();
			}
			else
			{
				unit->deleteLater();
			}

			update();
		}
	}
}

void GTile::mouseReleaseEvent(QMouseEvent *e)
{
	QWidget::mouseReleaseEvent(e);

	// --------- kliknutí ---------
	QPoint point = e->pos() - _mouseLastClick;
	if ( point.manhattanLength() < 3 )
	{
		_selected = ! _selected;

		bool valid = false;
		emit tileDidClicked(this, valid);

		if ( ! valid )
			setSelected( ! isSelected());
	}

	_mouseLastClick = QPoint();
	_mouseClick = false;
}

/**
 * Metoda pro zpracování Drop akce, pokud je pohyb validní, figurku přijme,
 * jinak figurku nepřijme.
 */
void GTile::dropEvent(QDropEvent *e)
{
	if ( e->mimeData()->hasFormat(GUnit::mimeData()) and _unit == NULL )
	{
		_drag = false;

		QString string(e->mimeData()->data(GUnit::mimeData()));
		GUnit * unit = GUnit::fromString(string, this);

		bool validMove = false;
		emit unitWillPutDown(this, validMove);

		if ( validMove )
		{
			_unit = unit;
			e->setDropAction(Qt::MoveAction);
			e->accept();

			_unit->show();
			updateUnitGeometry();
		}
		else
		{
			e->ignore();

			_unit = NULL;
			delete unit;

			setSelected(false);
		}

		update();
	}
}

