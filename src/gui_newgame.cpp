/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: GUI formulář pro vytvoření nové hry
 *
 * encoding: utf-8
 ******************************************************************************/

#include "gui_newgame.h"
#include "ui_gui_newgame.h"

GNewGame::GNewGame(QWidget *parent) : QDialog(parent), ui(new Ui::GNewGame)
{
	ui->setupUi(this);

	connect(ui->secondGroup, SIGNAL(buttonClicked(int)), this, SLOT(updateSecondLabel()));

	updateSecondLabel();
}

GNewGame::~GNewGame()
{
	delete ui;
}



int GNewGame::exec(Player ** local, Player ** second)
{
	int ret = QDialog::exec();

	if ( ret == QDialog::Accepted )
	{
		Player::Type localType, secondType;
		QString localName, secondName;

		if ( ui->buttonLocalComputer->isChecked() )
			localType = Player::Computer;
		else
			localType = Player::Human;


		localName = ui->lineLocalName->text();

		*local = Player::factoryPlayer(localType, localName);

		(*local)->setColor(ui->buttonBilaBarva->isChecked() ? ColorWhite : ColorBlack);


		if ( ui->buttonSecondComputer->isChecked() )
			secondType = Player::Computer;
		else if ( ui->buttonSecondHuman->isChecked() )
			secondType = Player::Human;
		else if ( ui->buttonSecondNetworkServer->isChecked() )
			secondType = Player::NetworkServer;
		else
			secondType = Player::NetworkClient;


		secondName = ui->lineSecondName->text();

		*second = Player::factoryPlayer(secondType, secondName);

		(*second)->setColor(inverseColor((*local)->color()));
	}

	return ret;

}


void GNewGame::updateSecondLabel()
{
	if ( ui->buttonSecondNetworkClient->isChecked() )
	{
		ui->label_3->setText("IP adresa:port");
	}
	else if ( ui->buttonSecondNetworkServer->isChecked() )
	{
		ui->label_3->setText("Port");
	}
	else
	{
		ui->label_3->setText(QString::fromUtf8("Jméno"));
	}
}

