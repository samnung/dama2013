
/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: implementace hry po síti
 *
 * encoding: utf-8
 ******************************************************************************/
#include "model_networkplayer.h"

#include <QHostAddress>
#include <QStringList>
#include <QDomElement>
#include <QDomDocument>

#include "model_move.h"


const char PROTOCOL_MOVE = 1;
const char PROTOCOL_INIT = 2;
const char PROTOCOL_END = 3;

const char PROTOCOL_NAME = 10;



NetworkPlayer::NetworkPlayer(const QString &hostadress, AbstractController * controller, QObject * parent)
	: Player(hostadress, controller, parent)
{
	_address = hostadress;
}


void NetworkPlayer::connectIfNeeded()
{
	_socket = new QTcpSocket;

	connect(_socket, SIGNAL(connected()), this, SLOT(socketConnected()));
	connect(_socket, SIGNAL(readyRead()), this, SLOT(readyForRead()));
	connect(_socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(connectProblem(QAbstractSocket::SocketError)));

	QHostAddress adress;
	int port = 0;

	if ( _address.contains(':') )
	{
		QStringList list = _address.split(':');
		adress.setAddress(list[0]);
		port = list[1].toInt();
	}
	else
	{
		adress.setAddress(_address);
	}

	_socket->connectToHost(adress, port);
}


void NetworkPlayer::moveWasPerformed(Coordinates from, Coordinates to)
{
	if ( _socket and _socket->isWritable() )
	{
		std::cerr << "sending move: from " << from << " to " << to << "\n";

		QByteArray array;

		array.append(PROTOCOL_MOVE);

		array.append(from.toNetworkFormat());
		array.append(to.toNetworkFormat());

		_socket->write(array);

	}
	else
		emit connectionRefused();

}
void NetworkPlayer::initBoardSetup(Moves &moves)
{
	if ( _socket and _socket->isWritable() )
	{
		std::cerr << "sending init: color " << colorToString(_color) << " moves " << moves << "\n";

		QByteArray array;

		// číslo zprávy
		array.append(PROTOCOL_INIT);

		// svou barvu -> při příjmu se nastaví opačná barva pro sebe
		// viz protokol 2. Počáteční konfigurace hry
		array.append(_color == ColorBlack ? 0 : 1);

		array.append(moves.toNetworkFormat());

		_socket->write(array);
	}
	else
	{
		emit connectionRefused();
	}
}

void NetworkPlayer::end()
{
	if ( _socket and _socket->isWritable() )
	{
		std::cerr << "sending end\n";

		QByteArray array;

		array.append(PROTOCOL_END);

		_socket->write(array);
	}
}


void NetworkPlayer::nameOfOtherPlayerChanged(const QString &name)
{
	if ( _socket and _socket->isWritable() )
	{
		std::cerr << "sending name: " << name.toStdString() << "\n";

		QByteArray array;


		// --------- číslo protokolu --------------
		array.append(PROTOCOL_NAME);


		// --------- číslo délka jména --------------
		QDataStream data(&array, QIODevice::Append);
		data.setByteOrder(QDataStream::LittleEndian);

		data << static_cast<quint32>(name.toUtf8().length());


		// --------- jméno --------------
		array.append(name.toUtf8());


		_socket->write(array);

	}
	else
	{
		emit connectionRefused();
	}
}




void NetworkPlayer::readyForRead()
{
	QByteArray array = _socket->readAll();




	while ( array.size() != 0 )
	{
		char type = array.at(0);
		array.remove(0, 1);

		switch ( type )
		{
		// --- pohyb -----
		case PROTOCOL_MOVE:
		{

			Coordinates from, to;

			from = Coordinates::fromNetworkFormat(array);
			to = Coordinates::fromNetworkFormat(array);

			std::cerr << "recieved move: from " << from << " to " << to << "\n";

			emit makesMove(this, from, to);

			break;

		}
			// ----- init ------
		case  PROTOCOL_INIT:
		{
			char color = array[0];
			array.remove(0, 1);

			_color = color == 1 ? ColorBlack : ColorWhite;

			emit informationChanged();


			Moves moves = Moves::fromNetworkFormat(array);

			std::cerr << "recieved init: color " << colorToString(_color) << " moves " << moves << "\n";

			emit initBoardSetupRecieved(moves);

			break;
		}
			// ------- konec --------
		case PROTOCOL_END:
		{
			_socket->close();

			emit connectionEnded();

			break;

		}
		case PROTOCOL_NAME:
		{
			// --------- číslo délka jména --------------
			QDataStream data(&array, QIODevice::ReadOnly);
			data.setByteOrder(QDataStream::LittleEndian);

			quint32 size;
			data >> size;

			array.remove(0, 4);

			// ------ načtení jména -------------
			QString name = QString::fromUtf8(array.data(), size);

			array.remove(0, size);

			std::cerr << "recieved name: " << name.toStdString() << "\n";

			setName(name);

			break;

		}
		default:
			break;
		}
	}

}

bool NetworkPlayer::connected()
{
	return (_socket ? _socket->isWritable() : false);
}

void NetworkPlayer::socketConnected()
{
	emit connectionEstablished();
}
void NetworkPlayer::connectProblem(QAbstractSocket::SocketError error)
{
	if ( error != QAbstractSocket::RemoteHostClosedError )
		emit connectionRefused();
}





QDomElement NetworkPlayer::toXML(QDomDocument &doc) const
{
	QDomElement el = Player::toXML(doc);
	el.setAttribute("address", _address);
	return el;
}

void NetworkPlayer::updateWithXML(const QDomElement &el)
{
	_address = el.attribute("address");
}
