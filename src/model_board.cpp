/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05), Vojtěch Dvořáček (xdvora0y)
 *
 * Popis: metody pro operace nad  hrací deskou
 *
 * encoding: utf-8
 ******************************************************************************/

#include "model_board.h"



const int BOARD_OPPOSITE = BOARD_WIDTH - 1;

bool isItBlackCoordinate(Coordinate x, Coordinate y)
{
	if ( x % 2 == 0 )
		return y % 2 == 0;
	else
		return y % 2 == 1;
}

size_t mapFunction(Coordinate x, Coordinate y)
{
	if ( x >= 0 and x < BOARD_WIDTH and y >= 0 and y < BOARD_WIDTH )
		return x * BOARD_WIDTH + y;
	else
		return -1;
}
size_t mapFunction(Coordinates xy)
{
	return mapFunction(xy.x, xy.y);
}


Board::Board()
{
	initBoard();
	_logIndex = 0;
}

const Coordinates Board::rotated(Color unitColor, const Coordinates toBeRotated) const
{
		if( unitColor == ColorBlack )
			return toBeRotated.rotatedTowardsBoardWidth(BOARD_WIDTH);
		else
			return toBeRotated;
}


void Board::initBoard()
{
	// ------------------ přidat políčka na desku ------------------
	for (Coordinate i = 0; i < BOARD_WIDTH; ++i)
	{
		for (Coordinate j = 0; j < BOARD_WIDTH; ++j)
		{
			Tile * tile = new Tile(i, j, isItBlackCoordinate(i, j) ? ColorBlack : ColorWhite);
			_tiles.push_back(tile);
		}
	}


		// ------------------ přidat figurky na políčka ------------------
	for (Coordinate i = 0; i < BOARD_WIDTH; ++i)
	{
		for (Coordinate j = 0; j < 3; ++j)
		{
			if ( isItBlackCoordinate(i, j) )
			{
				Unit * unit = new Unit(ColorWhite, Unit::Stone);
				_tiles[mapFunction(i, j)]->setUnit(unit);
			}
		}

		for (Coordinate j = 5; j < BOARD_WIDTH; ++j)
		{
			if ( isItBlackCoordinate(i, j) )
			{
				Unit * unit = new Unit(ColorBlack, Unit::Stone);
				_tiles[mapFunction(i, j)]->setUnit(unit);
			}
		}
	}

	//barva na tahu
	_onTheMove = ColorWhite;
}


const Tile * Board::tile(Coordinate x, Coordinate y) const
{
	return _tiles.at(mapFunction(x, y));
}

const Tile * Board::tile(Coordinates xy) const
{
	return _tiles.at(mapFunction(xy.x, xy.y));
}
Tile * Board::_tile(Coordinate x, Coordinate y) const
{
	return _tiles.at(mapFunction(x, y));
}
Tile * Board::_tile(Coordinates xy) const
{
	return _tiles.at(mapFunction(xy.x, xy.y));
}

bool Board::canMakeMove(Coordinates from, Coordinates to)
{
	bool taking = false;


	// kontrola hranic hrací plochy
	if ( to.x < 0 or to.x > BOARD_WIDTH or
		to.y < 0 or to.y > BOARD_WIDTH )
		return false;

	if ( ! isItBlackCoordinate(to.x, to.y) )
		return false;

	// kontrola obsazenosti zdrojové souřadnice
	Tile * fromTile = _tile(from);
	if ( ! fromTile->isOccupied() )
		return false;

	//kontrola barvy na tahu
	if(fromTile->unit()->color() != _onTheMove)
		return false;

	// kontrola obsazenosti cílové souřadnice
	Tile * toTile = _tile(to);
	if ( toTile->isOccupied() )
		return false;

	Unit * fromUnit = fromTile->unit();

	//rotace souradnic, lze pouzit pouze pro vyhodnoceni pohybu
	Coordinates rotTo, rotFrom;
	rotFrom = this->rotated(_tile(from)->unit()->color(),from);
	rotTo = this->rotated(_tile(from)->unit()->color(), to);
//    // std::cout << "form" << from << "->" << rotFrom << std::endl;
 //   // std::cout << "to" << to << "->" << rotTo << std::endl;

	// kontrola prostredni souřadnice při přeskakování kamenem
	Coordinates diff = rotTo - rotFrom;
//    // std::cout << "dx " << diff.x << ": dy " << diff.y << std::endl;
	// kontrola při přeskakování dámou
	if (fromUnit->type() == Unit::Queen)
	{
			Units _unitsBetween = this->unitsBetween(from, to);
			if (_unitsBetween.size() > 1)
			return false;

			if (_unitsBetween.size() == 1)
				taking = true;

			if ( (_unitsBetween.size() > 0 ) and (_unitsBetween[0]->color() == fromUnit->color()) )
				return false;

	}
	else if ( abs(diff.x) == diff.y and diff.y == 2 )
	{
		Coordinates middle = (from + to) / 2;

		Tile * tileMiddle = _tile(middle);
		if ( tileMiddle->isOccupied() )
		{
			Unit * unitMiddle = tileMiddle->unit();
			if ( unitMiddle->color() == fromUnit->color() )
			{
	   //         // std::cout << "Stejna brava\n";
				return false;
			}
			taking = true;
	   //     // std::cout << "kamen bere\n";
		}
		else
			return false;

	}


	if (!taking and (hasTakingPriority(from) != NULL) )
	{
		// std::cout << "nerespektuje brani\n";
		return false;
	}
	Tiles testToBeTaken = tilesToBeTaken(tile(from));

	bool test = false;
	if( !testToBeTaken.empty() )
	{
		for(Tile * it1: testToBeTaken )
		{
			for(Unit * it2: unitsBetween(from, to))
			{
				if( it1->unit() == it2 )
				{
					test = true;
					break;
				}
			}
			if(test)
				break;
		}

		if( !test )
			return false;
	}

	   // std::cout << "respektuje brani\n";

//    // std::cout << "dx " << diff.x << ": dy " << diff.y << std::endl;

	if (! fromUnit->canMoveTo(diff,taking))
		return false;

	return true;
}


Tiles Board::getHint(Coordinates from, Coordinates & hasPriority)
{
	Tiles canMoveTo, toTake;
	Tile * another;
	hasPriority = Coordinates(-1,-1);
	Coordinates diff, norm;

	if ( ! tile(from)->isOccupied() )
		return Tiles();

	toTake = this->tilesToBeTaken(_tile(from));

	if( !toTake.empty() )
	{
		for(Tile * tmp: toTake )
		{
			try {

				diff = tmp->coordinates() - from;
				norm = diff / diff.absCoords().x;
				canMoveTo.push_back(_tile(from+diff+norm));
			} catch (...) {
				continue;
			}
		}
	}
	else if( ((another = hasTakingPriority(from)) != NULL) ) //jina jednotka ma prioritu kvuli brani
	{
		canMoveTo = this->tilesToBeTaken(another);
		hasPriority = another->coordinates();
	}
	else
	{
		for( Tile * _it: _tiles )
		{
			if( canMakeMove(from, _it->coordinates()) )
				canMoveTo.push_back(_it);
		}
	}

	return canMoveTo;
}


Moves Board::getHints(Color forColor)
{
	Tiles tiles = tilesByUnitColor(forColor);

	Coordinates priority;

	Moves moves;

	for ( Tile * tile : tiles )
	{
		Tiles hints = getHint(tile->coordinates(), priority);

		if ( priority.isNull() )
		{
			for ( Tile * hint : hints )
			{
				moves.push_back(new Move(tile->coordinates(), hint->coordinates()));
			}

		}

	}
	return moves;
}


Tiles Board::tilesByUnitColor(Color unitColor)
{
	Tiles tiles;

	for ( Tile * tile : _tiles )
	{
		if ( tile->isOccupied() and tile->unit()->color() == unitColor )
		{
			tiles.push_back(tile);
		}
	}

	return tiles;
}


Board::MoveError Board::makeMove(Coordinates from, Coordinates to, Coordinates & removed)
{
	if ( ! this->canMakeMove(from, to) )
	{
		return Board::MOVE_ERROR;
	}

	removed = Coordinates(-1,-1); //default, bez odebrani

	Tiles toBeTaken  = this->tilesToBeTaken(tile(from));

	Tile * fromTile = _tile(from);
	Tile * toTile = _tile(to);

	Unit * fromUnit = fromTile->unit();

	Coordinates diff = from - to;

	//pohyb kamene

	Unit * unitTaken = NULL;

	if ( fromUnit->type() == Unit::Stone )
	{
		 if ( abs(diff.y) == 1 )
			toTile->setUnit(fromUnit);

		 else if ( abs(diff.y) == 2 ) //kamen bere
		 {
			removed = (from + to) / 2;
			unitTaken = _tile(removed)->removeUnit();
			toTile->setUnit( fromUnit );
		 }
	}
	else //pohyb damy
	{

		Tiles tBetween = this-> tilesBetween(from,to);
		Tile * isTaking = NULL;

		for( Tile * itTaken: toBeTaken )
		{
			//nastavi isTaking, pokud tah respektuje prioritu brani
			for( Tile * itBetween : tBetween )
			{
				if ( itBetween->coordinates() == itTaken->coordinates() )
					isTaking = itTaken ;
			}
		}

		if ((! toBeTaken.empty() ) and ( isTaking == NULL ) ) //melo by se brat a nebere
		{
			return Board::MOVE_ERROR;
		}
		else if( toBeTaken.empty() ) //neni co brat
		{
			 toTile->setUnit(fromUnit);
		}
		else //bere se
		{
			unitTaken = isTaking->removeUnit();
			removed = isTaking->coordinates();
			toTile->setUnit(fromUnit);
		}
	}

	//overeni dosazeni pozice damy
	Coordinates rotTo = this->rotated(fromTile->unit()->color(), toTile->coordinates());


	//pridani pohybu do logu
	bool unitChanged = false;

	if ( rotTo.y == BOARD_OPPOSITE) //protilehala strana plochy
	{
		fromUnit->setType( Unit::Queen );
		unitChanged = true;
	}

	Move * log = new Move(from, to, removed, unitTaken, fromTile->unit(), unitChanged);
	_moveLog.push_back(log); //pridani zaznamu do moves;

	fromTile->removeUnit(); //odebrani presunute jednotky

	_logIndex++;

	// ------ kontrola počtu figurek protihráče ------------
	if( tilesByUnitColor(inverseColor(_onTheMove)).empty() )
		return MOVE_WIN;


	_onTheMove = inverseColor(_onTheMove);

	if( unitChanged )
		return Board::MOVE_CHANGE;


return Board::MOVE_OK;
}

Tiles Board::tilesBetween(Coordinates from, Coordinates to) const
{
	if (  from == to or ! from.isDiagonal(to) )
		return Tiles();

	Tiles tiles;
	Coordinate dx = (from.x < to.x) ? 1 : -1;
	Coordinate dy = (from.y < to.y) ? 1 : -1;

	Coordinates i = from;

	while (true)
	{
		i.x += dx; i.y += dy;

		if ( i == to )
			break;

		tiles.push_back(this->_tile(i));
	}

	return tiles;
}

Units Board::unitsBetween(Coordinates from, Coordinates to)const
{
	if ( from == to )
		return Units();

	Units units;
	Tiles tiles = this->tilesBetween(from, to);

	for (Tile * tile : tiles)
	{
		if (tile->isOccupied())
			units.push_back(tile->unit());
	}

	return units;
}



Tiles Board::tilesToBeTaken(const Tile * activeTile)const
{
	Tiles toBeTaken; //vraci se, prazdne pokud neni co brat

	Unit * activeUnit = activeTile->unit();
	Coordinates rotatedInsp, rotatedActive,tmp, diff, norm, rotdiff, rotnorm;

	for(Tile * inspTile : _tiles)
	{
		Unit * inspUnit = inspTile->unit();

		if(inspUnit == NULL)
		{
			continue;
		}
		else if ( inspUnit->color() != activeUnit->color() ) //vybira pouze superovy jednotky v iteraci
		{
			if( inspTile->coordinates().isDiagonal(activeTile->coordinates()) )
			{
				//otoceni souradnic
				rotatedInsp = this->rotated(activeUnit->color(), inspTile->coordinates());
				rotatedActive = this->rotated(activeUnit->color(), activeTile->coordinates());

				rotdiff = rotatedInsp - rotatedActive;
				rotnorm = rotdiff / rotdiff.absCoords().x;

				diff = inspTile->coordinates() - activeTile->coordinates();
				norm = diff / diff.absCoords().x;

				try //chyta testy mimo hraci plochu
				{
					if( ! ((tile(inspTile->coordinates() + norm))->isOccupied()) ) //pripocte jednotkovy smer
					{
						if( this->unitsBetween( inspTile->coordinates(),activeTile->coordinates()).empty() )
						{
							if( activeTile->unit()->canMoveTo(rotdiff + rotnorm, true) )
							{
								toBeTaken.push_back(inspTile);
							}
						}
					}
					else
						continue;

				}
				catch(...)
				{
					continue;
				}
			}
		}
	}

	return toBeTaken;
}



Tile * Board::hasTakingPriority(const Coordinates from)
{
	Tiles toBeTaken;
	const Tile * fromTile = tile(from);

   // std::cout << "entering HasTP\n" << std::endl;

	for ( Tile * _iter: _tiles ) //kontroluje prioritu brani pro vsechny spoluhrace
	{
		if( (_iter != _tile(from)) and (_iter->unit() != NULL) )
		{
			if( _iter->unit()->color() == fromTile->unit()->color() )
			{
			//    // std::cout << _iter << std::endl;
//          // std::cout << _iter->unit()->color() << std::endl;

				toBeTaken  = this->tilesToBeTaken(_iter);
  //              // std::cout << "size" << toBeTaken.size() << std::endl;
				if( ! toBeTaken.empty())
				{

				// std::cout << "ret"<< _iter->coordinates() << std::endl;
					return _iter;
				}
			}
		}
	}
   // std::cout << "hasTP: ret NULL" << std::endl;
	return NULL;
}


bool Board::loadGame(Moves movesList)
{
	Coordinates blind;

	for(Move * mv: movesList)
	{
		if( makeMove(mv->from(), mv->to(), blind) == MOVE_ERROR )
			return false;
	}

	return true;
}

Move * Board::nextMove()
{
	Unit * tmp;

	if( _logIndex != _moveLog.size() )
	{
		Move * move = _moveLog.at(_logIndex);

		tmp = _tile( move->from() )->removeUnit();
		_tile( move->to() )->setUnit(tmp);

		if ( ! move->taken().isNull() )
			_tile( move->taken() )->removeUnit();

		if( move->unitChanged() )
			_tile( move->to() )->unit()->setType(Unit::Queen);

		_logIndex++;

		return move;
	}

	return NULL;
}
Move * Board::prevMove()
{
	Unit * tmp;

	if( _logIndex != 0 )
	{
		_logIndex--;

		Move * move = _moveLog.at(_logIndex);

		tmp = _tile( move->to() )->removeUnit();
		_tile( move->from() )->setUnit(tmp);

		if( ! move->taken().isNull() )
			_tile( move->taken() )->setUnit(move->takenUnit());

		if( move->unitChanged() )
			_tile( move->from() )->unit()->setType(Unit::Stone);

		return move;
	}
	return NULL;

}





Tiles Board::tilesWithPossibleMove()
{
	Tiles tiles;

	Moves moves = getHints(colorOnTheMove());
	for ( Move * move : moves )
	{
		tiles.push_back(_tile(move->from()));
	}

	return tiles;
}

Tiles Board::tilesWhereCanMove(Coordinates from)
{
	Tiles tiles;

	Coordinates priority;
	tiles = getHint(from, priority);

	if ( priority.isNull() )
		return tiles;
	else
		return Tiles();

}

/**
 * Funkce pro výpis horní mřížky do streamu
 * @param os výstupní stream
 */
void printTopTableDelimiter(std::ostream & os)
{
	os << "+";

	for (Coordinate x = 0; x < BOARD_WIDTH - 1; x++)
	{
		os << "---+";
	}

	os << "---+" << std::endl;
}

/**
 * Funkce pro výpis spodní mřížky do streamu
 * @param os výstupní stream
 */
void printBottomTableDelimiter(std::ostream & os)
{
	os << "+";

	for (Coordinate x = 0; x < BOARD_WIDTH - 1; x++)
	{
		os << "---+";
	}

	os << "---+" << std::endl;
}

/**
 * Funkce pro výpis střední mřížky do streamu
 * @param os výstupní stream
 */
void printMiddleTableDelimiter(std::ostream & os)
{
	os << "+";

	for (Coordinate x = 0; x < BOARD_WIDTH - 1; x++)
	{
		os << "---+";
	}

	os << "---+" << std::endl;
}

std::ostream & operator << (std::ostream & os, const Board & board)
{
	os << "  ";
	printTopTableDelimiter(os);

	for (Coordinate y = BOARD_WIDTH - 1; y >= 0; y--)
	{
		os << y + 1 << " |";

		for (Coordinate x = 0; x < BOARD_WIDTH; x++)
		{

			const Tile * tile = board.tile(x, y);

			if ( tile->isOccupied() )
			{
				Unit *unit = tile->unit();
				os << *unit;
			}
			else
				os << "   ";

			if ( x < BOARD_WIDTH - 1 )
				os << "|";
		}

		os << "|" << std::endl;

		if ( y > 0 ){
			os << "  ";
			printMiddleTableDelimiter(os);
		}
	}
	os << "  ";
	printBottomTableDelimiter(os);

	os << "    A   B   C   D   E   F   G   H\n";

	return os;
}

