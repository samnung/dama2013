/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05), Vojtěch Dvořáček (xdvora0y)
 *
 * Popis: třída pro záznam a práci s pohybem
 *
 * encoding: utf-8
 ******************************************************************************/

#ifndef MODEL_MOVE_H
#define MODEL_MOVE_H

#include "model_global.h"

class QDomElement;
class QDomDocument;
class Unit;

/**
 * @class Move
 *
 * Třída představující jeden tah
 */
class Move
{
	Coordinates _from;
	Coordinates _to;
	Coordinates _taken;
	Unit * _takenUnit;
	Unit * _activeUnit;
	bool _unitChanged;

public:
	Move() : _from(), _to(), _taken() {_activeUnit = NULL; _takenUnit = NULL; _unitChanged = false; }
	Move(Coordinates from,
		 Coordinates to,
		 Coordinates taken = Coordinates(),
		 Unit * takenUnit = NULL,
		 Unit * activeUnit = NULL,
		 bool changed = false)
		: _from(from), _to(to), _taken(taken), _takenUnit(takenUnit), _activeUnit(activeUnit), _unitChanged(changed) {}

	// getters
	const Coordinates & from() const { return _from; }
	const Coordinates & to() const { return _to; }
	const Coordinates & taken() const { return _taken; }
	Unit * activeUnit() const { return _activeUnit; }
	Unit * takenUnit() const { return _takenUnit; }
	bool unitChanged() const { return _unitChanged; }

	// převodní metody
	QString toString() const;

	friend std::ostream & operator << (std::ostream & os, const Move & move);

	QByteArray toNetworkFormat();
	static Move * fromNetworkFormat(QByteArray & array);

	QDomElement toXML(QDomDocument & doc);
	static Move * fromXML(const QDomElement & element);

	QByteArray toStandardFormat();
	static Move * fromStandardFormat(QByteArray & array);
};


/**
 * @class Moves
 *
 * Třída přestavující seznam tahů
 */
class Moves : public std::vector<Move *>
{

public:

	// převodní metody
	friend std::ostream & operator << (std::ostream & os, Moves & moves);

	QByteArray toNetworkFormat();
	static Moves fromNetworkFormat(QByteArray & array);

	QDomElement toXML(QDomDocument & doc);
	static Moves fromXML(const QDomElement & element);

	QByteArray toStandardFormat();
	static Moves fromStandardFormat(const QByteArray & array);
};




#endif // MODEL_MOVE_H
