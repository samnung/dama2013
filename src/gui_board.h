/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: třída popisující GUI hrací desky
 *
 * encoding: utf-8
 ******************************************************************************/

#ifndef GUI_BOARD_H
#define GUI_BOARD_H

#include <QWidget>
#include <QLabel>

#include "gui_tile.h"
#include "model_abstractcontroller.h"


class Board;


typedef QVector <QLabel *> QLabelList;
typedef QVector <GTile *> GTileList;



/**
 * @class GBoard
 *
 * Třída zajišťuje zobrazení desky na obrazovce.
 *
 * Stará se o zobrazování desky, využívá GTile pro vykreslení jednotlivých políček
 * následně GUnit pro popis grafických jednotek (kamene, dámy).
 *
 * Deska dokáže vykreslovat nápovědu, využívá pro to AbstractController, od kterého získává
 * souřadnice jednotlivých políček, které má zvýraznit. Pro ovládání zobrazování nápovědy se
 * použije metoda setShowHints().
 */
class GBoard : public QWidget
{
	Q_OBJECT

public:
	/**
	 * Konstruktor pro vytvoření grafické desky
	 * @param board			reference na model desky, podle které se inicializuje rozmístění figurek
	 * @param controller	ukazatel na controller, pro vzájemnou komunikaci, mohlo být vyřešeno pomocí signálů
	 * @param parent		QWiget parent
	 */
	explicit GBoard(const Board & board, AbstractController * controller, QWidget * parent = 0);


	/**
	 * Provede přemístěni figurky z místa na místo
	 *
	 * @param from		původní pozice
	 * @param to		konečná pozice
	 */
	void makeMove(Coordinates from, Coordinates to);

	/**
	 * Provede odstranění figurky z daného místa
	 *
	 * @param from		původní pozice
	 */
	void removeUnit(Coordinates from);

	/**
	 * Přidání grafické figurky na desku
	 *
	 * @param to		souřadnice kam se má figurka vložit
	 * @param unit		modelová figurka, podle které se vytvoří grafická figurka a vloží na danou souřadnici -> políčko
	 */
	void addUnit(Coordinates to, Unit * unit);

	/**
	 * Aktualizace jednotky na určené souřadnici
	 *
	 * @param where		souřadnice figurky, která se má aktualizovat
	 * @param by		figurka, podle které se má grafická figurka aktualizovat
	 */
	void updateUnit(Coordinates where, Unit * by);

	/**
	 * Metoda pro označení políček danou barvou
	 *
	 * @param tiles		seznam políček pro zvýraznění
	 * @param color		barva, která se použije pro zvýraznění
	 */
	void highlightTiles(Tiles tiles, QColor color);

	/**
	 * Aktulizace zvýraznění políček
	 *
	 * Zobrazuje možné tahy pro aktuálního hráče, pokud se snaží o tah, zobrazuje místa, na které může vložit vybranou figurku
	 */
	void updateHighlights();


	/**
	 * Nastaví u všech políček danou hodnotu zvýraznění
	 * @param selected		false == nezvýrazněné
	 */
	void setAllTilesUnselected(bool selected = false);

	/**
	 * Metoda pro nastavení, jestli má deska zobrazovat nápovědu
	 *
	 * Využívá metody:
	 *		- AbstractController::tilesWithPossibleMove() pro seznam políček, ze kterých se dá táhnout
	 *		- AbstractController::tilesWhereCanMove(Coordinates from) pro seznam, do kterých se může jít z dané souřadnice
	 *
	 * @param showHints		true == zobrazuje nápovědy
	 */
	void setShowHints(bool showHints);


protected:
	/**
	 * Aktualizuje rozměry grafických políček
	 */
	void resizeEvent(QResizeEvent *);

private:

	AbstractController * _controller;

	qreal _boardWidth;

	GTileList _tiles;

	GTile * _fromTile;
	GTile * _toTile;

	bool _movingUnit;
	bool _showHints;


	// legenda desky + rámeček
	QLabelList _vertical;
	QLabelList _horizontal;
	QFrame _frame;


	// aktualizace figurky
	Unit * _updateUnitBy;
	Coordinates _updateUnitWhere;






private slots:
	/**
	 * Slot, pro obsluhu kliknutí na políčko
	 *
	 * Určí se, zda vůbec může vybrat, popřípadě, jestli už nevybral druhé políčko a tímpádem se jedná o pohyb
	 * @param tile		ukazatel na políčko
	 * @param validity	zpětná vazba validace zvýraznění
	 */
	void tileWillSelect(GTile * tile, bool & validity);

	/**
	 * Slot, pro obsluhu zvednutí figurky z políčka
	 * @param self		ukazatell na políčko, ze kterého se zvedlo
	 * @param validity	zpětná vazba validace
	 */
	void unitWillPickUp(GTile *self, bool & validity);

	void unitDidPickUp(GTile *self);

	void unitWillPutDown(GTile *self, bool & validity);

	/**
	 * Používá se pro opožděnou aktualizaci figurky (Drag & Drop ještě neskončil)
	 */
	void _updateUnit();
};

#endif // GUI_BOARD_H
