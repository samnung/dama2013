/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: třída zakladajícího síťového hráče
 *
 * encoding: utf-8
 ******************************************************************************/

#ifndef MODEL_SERVERPLAYER_H
#define MODEL_SERVERPLAYER_H

#include "model_networkplayer.h"

class QTcpServer;
class QTcpSocket;


/**
 * Serverový hráč
 *
 * Přidává možnost vytvořit server pro komunikaci, jinak se chová stejně jako Síťový hráč (NetworkPlayer).
 */
class ServerPlayer : public NetworkPlayer
{
	Q_OBJECT

	QTcpServer * _server;

public:
	/**
	 * Kontruktor pro serverového hráče
	 * @param port			port na kterém má poslouchat (pro zjednodušení dědičnosti se použil QString)
	 * @param controller	ukazatel na controller
	 * @param parent		QObject parent
	 */
	ServerPlayer(QString port, AbstractController * controller = NULL, QObject * parent = 0);


	virtual QString prettyName() const { return name() + " (server)"; }

	/**
	 * Metoda pro zjištění typu hráče
	 * @return typ hráče
	 */
	virtual Type type() const override { return NetworkServer; }

	/**
	 * Metoda pro vytvoření serveru
	 *
	 * Začne poslouchat a čeká dokud se nepřipojí klient, poté vyšle signál connectionEstablished()
	 */
	virtual void connectIfNeeded() override;

private slots:

	/**
	 * Slot, který se vyvolá při uskutečnění připojení (klient se na nás připojil)
	 */
	void newConnection();

};

#endif // MODEL_SERVERPLAYER_H
