/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: metody třídy player
 *
 * encoding: utf-8
 ******************************************************************************/

#include "model_player.h"

#include "model_aiplayer.h"
#include "model_networkplayer.h"
#include "model_serverplayer.h"


#include <QDomDocument>
#include <QDomElement>


const QString PLAYER_HUMAN = "local-human";
const QString PLAYER_COMPUTER = "local-computer";
const QString PLAYER_NETWORK_CLIENT = "remote-client";
const QString PLAYER_NETWORK_SERVER = "remote-server";



QString Player::typeToString(Type type)
{
	switch (type)
	{
	case Human: return PLAYER_HUMAN; break;
	case Computer: return PLAYER_COMPUTER; break;
	case NetworkClient: return PLAYER_NETWORK_CLIENT; break;
	case NetworkServer: return PLAYER_NETWORK_SERVER; break;
	default: return QString(); break;
	}
}

Player::Type Player::stringToType(const QString & str)
{
	if ( str == PLAYER_COMPUTER )
		return Computer;
	else if ( str == PLAYER_NETWORK_CLIENT )
		return NetworkClient;
	else if ( str == PLAYER_NETWORK_SERVER )
		return NetworkServer;
	else
		return Human;
}








Player::Player(const QString & name, AbstractController * controller, QObject * parent) : QObject(parent), _name(name)
{
	_controller = controller;
}


void Player::connectIfNeeded()
{
	emit connectionEstablished();
}

bool Player::connected()
{
	return true;
}


void Player::setName(const QString & name)
{
	_name = name;
	emit informationChanged();
}
void Player::setColor(const Color &color)
{
	_color = color;
	emit informationChanged();
}







void Player::moveWasPerformed(Coordinates , Coordinates )
{

}
void Player::initBoardSetup(Moves &)
{

}
void Player::end()
{

}

void Player::nameOfOtherPlayerChanged(const QString &)
{

}


QDomElement Player::toXML(QDomDocument &doc) const
{
	QDomElement element = doc.createElement("player");
	element.setAttribute("type", Player::typeToString(type()));
	element.setAttribute("name", _name);
	element.setAttribute("color", QString::fromStdString(colorToString(color())));

	return element;
}

void Player::updateWithXML(const QDomElement &)
{

}

Player * Player::fromXML(const QDomElement &element)
{
	if ( element.tagName() == "player" )
	{
		Type type = stringToType(element.attribute("type"));

		if ( type == NetworkClient )
			type = NetworkServer;

		QString name = element.attribute("name");
		Color color = colorFromString(element.attribute("color").toStdString());

		Player * player = factoryPlayer(type, name);
		player->setColor(color);
		player->updateWithXML(element);

		return player;
	}

	return NULL;
}





Player * Player::factoryPlayer(Type type, const QString &name)
{
	switch (type)
	{
	case Human:
		return new Player(name);
		break;

	case Computer:
		return new AIPlayer(name);
		break;

	case NetworkClient:
		return new NetworkPlayer(name);
		break;

	case NetworkServer:
		return new ServerPlayer(name);
		break;

	default:
		return NULL;
		break;
	}
}
