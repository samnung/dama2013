/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: třída popisující AI hráče
 *
 * encoding: utf-8
 ******************************************************************************/

#ifndef MODEL_AIPLAYER_H
#define MODEL_AIPLAYER_H

#include "model_player.h"


/**
 * @class AIPlayer
 *
 * Třída popisující hráče s "umělou inteligencí"
 *
 * Umělá inteligence spočívá v získání seznamu možných tahů a náhodného vyběru
 *
 * Pro zpestření je použit časovač, který má pokaždé jiný čas vypršení (od 500 do 2000 ms).
 */
class AIPlayer : public Player
{
	Q_OBJECT

public:
	/**
	 * Konstruktor pro hráče s "umělou inteligencí"
	 * @param name			jméno hráče
	 * @param controller	ukazatel na controller (používá se pro získání nápovědy)
	 * @param parent		object parent
	 */
	AIPlayer(const QString & name, AbstractController * controller = NULL, QObject * parent = 0);


	/**
	 * Metoda pro získání jména, které se použije pro výpis
	 * @return	jméno hráče + informace jakého typu je hráč
	 */
	virtual QString prettyName() const { return name() + QString::fromUtf8(" (počítač)"); }

	/**
	 * Metoda pro zjištění typu hráče
	 * @return typ hráče
	 */
	virtual Type type() const override { return Computer; }

	/**
	 * Metoda pro oznamování, že byl proveden tah
	 *
	 * Hráč po nějaké době vyšle signál, že také provedl tah
	 *
	 * @param from		počáteční souřadnice tahu
	 * @param to		koncová souřadnice tahu
	 */
	virtual void moveWasPerformed(Coordinates from, Coordinates to) override;


private slots:
	/**
	 * Slot, který provede tah za počítač, volá se časovačem pro získání zpoždění
	 */
	void _makeMove();

};

#endif // MODEL_AIPLAYER_H
