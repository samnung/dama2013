/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05), Vojtěch Dvořáček (xdvora0y)
 *
 * Popis: implementaci operací nad dvourozměrnými souřadnicemi
 *
 * encoding: utf-8
 ******************************************************************************/

#ifndef MODEL_GLOBAL_H
#define MODEL_GLOBAL_H

#include <iostream>
#include <vector>
#include <set>
#include <sstream>
#include <cstdlib>
#include <algorithm>
#include <cctype>

#include <QByteArray>
#include <QString>
#include <QTime>


// fix bug in SublimeClang
#ifndef NULL
 #define NULL 0
#endif



#define APP_NAME "dama2013"




#define randomNumber(low, high) (qrand() % ((high + 1) - low) + low)



typedef int Coordinate;

/**
 * Struktura představující dvou rozměrné souřadnice
 */
struct Coordinates
{
	Coordinate x, y;

	/**
	 * Konstruktor, který uvede souřadnice do stavu Null
	 */
	Coordinates() : x(-1), y(-1) {}
	Coordinates(Coordinate _x, Coordinate _y) : x(_x), y(_y) {}

	Coordinates operator - (Coordinates & diff)
	{
		return Coordinates(this->x - diff.x, this->y - diff.y);
	}
	Coordinates operator - (const Coordinates & diff) const
	{
		return Coordinates(this->x - diff.x, this->y - diff.y);
	}

	Coordinates operator + (Coordinates & plus) const
	{
		return Coordinates(this->x + plus.x, this->y + plus.y);
	}
	Coordinates operator / (int number)
	{
		return Coordinates(this->x / number, this->y / number);
	}
	bool operator == (const Coordinates & equal) const
	{
		return (this->x == equal.x and this->y == equal.y);
	}
	bool operator != (Coordinates & equal)
	{
		return ! ( this->operator == (equal) );
	}
	bool operator > (int value)
	{
		return (this->x > value and this->y > value);
	}
	bool operator < (int value)
	{
		return (this->x < value and this->y < value);
	}
	bool operator >= (int value)
	{
		return (this->x >= value and this->y >= value);
	}

	std::string toString() const
	{
		std::stringstream ss;
		ss << *this;
		return ss.str();
	}

	/**
	 * Převod na standardní notaci
	 * @return řetězec, který má formát "a1" pro souřadnici 0, 0
	 */
	QByteArray toStandardFormat()
	{
		return QString::fromStdString(toString()).toLocal8Bit();
	}

	/**
	 * Převod ze standradní notace
	 * @param array řetezec, který se !!! zkracuje !!!
	 * @return vytvořenou souřadnici
	 */
	static Coordinates fromStandardFormat(QByteArray & array)
	{
		Coordinates coor;
		if ( array.size() >= 2 and isalpha(array.at(0)) and isdigit(array.at(1)) )
		{
			coor.x = tolower(array.at(0)) - 'a';
			coor.y = array.at(1) - '0' - 1;
			array.remove(0, 2);
		}

		return coor;
	}

	/**
	 * Převod ze standradní notace
	 * @param array řetezec, který se !!! zkracuje !!!
	 * @return vytvořenou souřadnici
	 */
	static Coordinates fromStandardFormat(std::string & array) //pouziva CLI
	{
		Coordinates coor;
		if ( array.length() >= 2 and isalpha(array.at(0)) and isdigit(array.at(1)) )
		{
			coor.x = tolower(array.at(0)) - 'a';
			coor.y = array.at(1) - '0' - 1;
			array.erase(0, 3);
		}

		return coor;
	}

	/**
	 * Metoda pro otočení souřadnice vůči desce
	 * @param width šířka desky
	 * @return otočenou souřadnici (např: a1 -> h8)
	 */
	Coordinates rotatedTowardsBoardWidth(Coordinate width) const
	{
		return Coordinates(width - this->x - 1, width - this->y - 1);
	}

	friend std::ostream & operator << (std::ostream & os, const Coordinates & coor)
	{
		os << static_cast<char>(coor.x + 'a') << coor.y + 1;
		return os;
	}



	/**
	 * Převod souřadnice pro síťový přenos
	 * @return array dvou binárních čísel ( a1 == [0x00,0x00] )
	 */
	QByteArray toNetworkFormat() const
	{
		QByteArray array;
		array.append(static_cast<char>(this->x));
		array.append(static_cast<char>(this->y));
		return array;
	}

	/**
	 * Převod souřadnice ze síťového přenosu
	 * @param array		vstupní pole
	 * @return
	 */
	static Coordinates fromNetworkFormat(QByteArray & array)
	{
		Coordinates coor;
		coor.x = array[0];
		array.remove(0, 1);

		coor.y = array[0];
		array.remove(0, 1);

		return coor;
	}

	/**
	 * Převod na XML hodnotu
	 * @return řetězec, kde pro souřadnici a1 je "0000", a2 "0001", b3 "0102", ...
	 */
	QString toXMLValue()
	{
		return QString("%1%2").arg(x, 2, 10, QLatin1Char('0')).arg(y, 2, 10, QLatin1Char('0'));
	}

	/**
	 * Vytvoření souřednice z řetězce
	 * @param str vstupní řetězec v formátu čtyř čísel, viz toXMLValue()
	 * @return vytvořenou souřadnici, pokud je validní, není nastavena na Null, viz isNull()
	 */
	static Coordinates fromXMLValue(const QString & str)
	{
		Coordinates coor;
		coor.x = str.mid(0, 2).toInt();
		coor.y = str.mid(2, 2).toInt();

		return coor;
	}

	/**
	 * Převod na absolutní hodnoty
	 * @return souřadnice s absolutnímí hodnotami
	 */
	Coordinates absCoords()
	{
		return Coordinates(abs(this->x),abs(this->y));
	}

	/**
	 * Pro zjištění, jestli je jsou dvě souřadnice diagonálně
	 * @param dest druhá souřadnice
	 * @return true pokud jsou souřadnice navzájem diagonálně
	 */
	bool isDiagonal(const Coordinates & dest) const
	{
		Coordinates diff;

		diff.x = abs(this->x - dest.x);
		diff.y = abs(this->y - dest.y);

		return (diff.x == diff.y);
	}
	/**
	 * Pro zjištění validity souořadnice
	 * @return false pokud jsou obě souřadnice -1, jinak true
	 */
	bool isNull() const
	{
		return (x == -1 and y == -1);
	}
};




enum Color { ColorWhite, ColorBlack };

const std::string ColorWhiteString = "W";
const std::string ColorBlackString = "B";

extern Color inverseColor(Color color);
extern const std::string & colorToString(Color color);
extern Color colorFromString(const std::string & str);



/**
 * Velikost desky, pokud se změní, změní se všechno, ale stále to funguje, kromě inicializace desky, černý hráč má více figurek :(
 */
const Coordinate BOARD_WIDTH = 8;




#define MIN(a,b) ((a > b) ? (b) : (a))
#define MAX(a,b) (!(a > b) ? (b) : (a))




#endif
