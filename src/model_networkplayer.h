/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: třída představující síťového hráče
 *
 * encoding: utf-8
 ******************************************************************************/

#ifndef MODEL_NETWORKPLAYER_H
#define MODEL_NETWORKPLAYER_H

#include <QTcpSocket>

#include "model_player.h"

/**
 * Síťový hráč, který se připojuje se serverového hráče
 */
class NetworkPlayer : public Player
{
	Q_OBJECT

protected:
	QTcpSocket * _socket;
	QString _address;

public:
	/**
	 * Kontruktor síťového hráče, v ihned se snaží připojit na hosta
	 * @param hostadress	cílová adresa, na kterou se má hráč připojit
	 * @param controller	ukazatel na controller
	 * @param parent		QObject parent
	 */
	NetworkPlayer(const QString & hostadress, AbstractController * controller = NULL, QObject * parent = 0);


	virtual QString prettyName() const { return name() + " (klient)"; }

	/**
	 * Metoda pro zjištění typu hráče
	 * @return typ hráče
	 */
	virtual Type type() const override { return NetworkClient; }

	/**
	 * Metoda pro oznamování, že byl proveden tah
	 *
	 * @param from		počáteční souřadnice tahu
	 * @param to		koncová souřadnice tahu
	 */
	virtual void moveWasPerformed(Coordinates from, Coordinates to) override;

	/**
	 * Metoda pro vytvoření spojení
	 *
	 * Pokusí se o spojení na hosta
	 */
	virtual void connectIfNeeded() override;

	/**
	 * Metoda pro zjištění, zda je hráč připojen
	 * @return stav připojení
	 */
	virtual bool connected() override;

	/**
	 * Oznámení, že úvodní stav bude jiný, na základě seznamu pohybů
	 *
	 * @param moves		seznam pohybů, které je nutné vykonat pro synchronizaci desek
	 */
	virtual void initBoardSetup(Moves &moves) override;

	/**
	 * Oznámení konce hry
	 */
	virtual void end() override;


	/**
	 * Oznámení o změně jména
	 */
	virtual void nameOfOtherPlayerChanged(const QString & name);


	/**
	 * Metoda pro uložení informací hráče do XML
	 * @param doc	vstupní dokument pro vytváření elementů
	 * @return vytvořený element
	 */
	virtual QDomElement toXML(QDomDocument & doc) const;


	/**
	 * Metoda pro aktualizace hráče z XML, kvůli polymorfismu
	 * @param el
	 */
	virtual void updateWithXML(const QDomElement & el);



private slots:

	/**
	 * Slot, který se vyvolá při uskutečnění připojení
	 */
	void socketConnected();

	/**
	 * Slot, pro zpracování přijatých dat
	 */
	void readyForRead();

	/**
	 * Slot, pro obsloužení chyby při připojování
	 *
	 * vyšle signál connectionRefused()
	 */
	void connectProblem(QAbstractSocket::SocketError error);

};

#endif // MODEL_NETWORKPLAYER_H
