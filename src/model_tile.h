/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05), Vojtěch Dvořáček (xdvora0y)
 *
 * Popis: Třída představující políčko na desce
 *
 * encoding: utf-8
 ******************************************************************************/

#ifndef MODEL_TILE_H
#define MODEL_TILE_H

#include "model_global.h"
#include "model_unit.h"


/**
 * @class Tile
 *
 * Třída představující jedno políčko v modelu
 */
class Tile
{
	Coordinates _position;

	Color _color;

protected:
	Unit *_unit;

public:
	/**
	 * Konstruktor pro vytvoření políčka
	 * @param x		souřadnice
	 * @param y		souřadnice
	 * @param color	barva políčka
	 * @param unit	ukazatel na figurku, kterou nese
	 */
	Tile(Coordinate x, Coordinate y, Color color, Unit * unit = NULL);

	/**
	 * Konstruktor pro vytvoření políčka
	 * @param xy	souřadnice
	 * @param color	barva políčka
	 * @param unit	ukazatel na figurku, kterou nese
	 */
	Tile(Coordinates coordinates, Color color, Unit * unit = NULL);


	void setUnit(Unit * unit) { _unit = unit; }
	Unit * removeUnit() { Unit *tmp = _unit; _unit = NULL; return tmp; }

	/**
	 * Ukazatel na políčko
	 * @return
	 */
	Unit * unit() const { return _unit; }

	/**
	 * Je políčko obsazeno ?
	 * @return obsazenost políčka
	 */
	bool isOccupied() const { return _unit != NULL; }

	/**
	 * Získání barvy
	 * @return barva políčka
	 */
	const Color & color() const { return _color; }

	/**
	 * Souřadnice políčka
	 * @return
	 */
	const Coordinates & coordinates() const { return _position; }


	friend std::ostream & operator << (std::ostream & os, const Tile & tile);

};


typedef std::vector<Tile *> Tiles;
typedef std::set<Tile *> TileSet;

extern std::ostream & operator << (std::ostream & os, const Tiles & tiles);

#endif
