/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: třída pro založení nové hry
 *
 * encoding: utf-8
 ******************************************************************************/

#ifndef GUI_NEWGAME_H
#define GUI_NEWGAME_H

#include <QDialog>


#include "model_global.h"
#include "model_player.h"


namespace Ui {
class GNewGame;
}

/**
 * @class GNewGame
 *
 * Dialogové okno pro vytvoření nové hry
 */
class GNewGame : public QDialog
{
	Q_OBJECT

public:
	/**
	 * Konstruktor
	 * @param parent
	 */
	explicit GNewGame(QWidget *parent = 0);
	~GNewGame();

	/**
	 * Otevře modálně okno
	 *
	 * Vytvoří instance na hráče, díky statické metodě Player::factoryPlayer().
	 *
	 * @param local			vrací ukazatel na nově vytvořenýho lokálního hráče
	 * @param second		vrací ukazatel na nově vytvořenýho druhého hráče
	 *
	 * @return vrací QDialog::Accepted nebo QDialog::Rejected
	 */
	int exec(Player ** local, Player ** second);


private:
	Ui::GNewGame *ui;

private slots:
	void updateSecondLabel();
};

#endif // GUI_NEWGAME_H
