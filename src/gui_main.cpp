/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: implementace main() pro GUI
 *
 * encoding: utf-8
 ******************************************************************************/

#include <QApplication>

#include "gui_mainwindow.h"

int main (int argc, char ** argv)
{
	QApplication app(argc, argv);

	qsrand((uint)QTime::currentTime().msec());

	MainWindow win;
	win.show();

	return app.exec();
}
