/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)i, Vojtěch Dvořáček (xdvora0y)
 *
 * Popis: Třída představující instanci figurky na desce
 *
 * encoding: utf-8
 ******************************************************************************/

#ifndef MODEL_UNIT_H
#define MODEL_UNIT_H

#include "model_global.h"
#include <cstdlib>


/**
 * Třída představující figurku
 */
class Unit
{
public:
	enum Type { Stone, Queen };

protected:
	Color _color;
	Type _type;

public:
	Unit(Color color, Type type) : _color(color), _type(type) {}

	/**
	 * Zjistí validitu pohybu pro současnou figurku
	 *
	 * Každá figurka má omezené pohyby:
	 * 		- Kámen může jen o jeden krok dopredu v diagonálním směru.
	 *		- Dáma může jen diagonálním směrem.
	 *
	 * @param x			složka vektoru pohybu
	 * @param y			složka vektoru pohybu
	 * @param taking	informace, zda při tomto pohybu bere jednotku
	 *
	 * @return validitu pohybu
	 */
	bool canMoveTo(Coordinate x, Coordinate y, bool taking = false); //@arg1,2 slozky vektoru pohybu

	/**
	 * Zjistí validitu pohybu pro současnou figurku
	 *
	 * @see canMoveTo(Coordinate x Coordinate y, bool taking)
	 */
	bool canMoveTo(Coordinates xy, bool taking = false); //@arg1: vektor pohyb


	Color color() const { return _color; }
	Type type() const { return _type; }

	void setType(Type type) { _type = type; }

	/**
	 * Vytvoření instance figurky z řetězce
	 *
	 * Formát je " B " pro černý kámen, "*B*" pro dámu, stejné pro další barvu, ale místo B(black) je W (white)
	 * @param string vstupní řetězec
	 * @return instanci figurky
	 */
	static Unit * fromString(const std::string & string);

};

typedef std::vector<Unit *> Units;

extern std::ostream & operator << (std::ostream & os, const Unit & unit);


#endif
