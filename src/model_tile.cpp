/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05), Vojtěch Dvořáček (xdvora0y)
 *
 * Popis: Třída představující políčko na desce
 *
 * encoding: utf-8
 ******************************************************************************/

#include <iostream>

#include "model_tile.h"


Tile::Tile(Coordinates coordinates, Color color, Unit * unit) : _position(coordinates), _color(color)
{
	_unit = unit;
}

Tile::Tile(Coordinate x, Coordinate y, Color color, Unit * unit) : _position(Coordinates(x, y)), _color(color)
{
	_unit = unit;
}


std::ostream & operator << (std::ostream & os, const Tile & tile)
{
	os << tile._position;
	return os;
}

std::ostream & operator << (std::ostream & os, const Tiles & tiles)
{
    for(Tile * it: tiles)
    {
        os << *it;
    }
    return os;
}


