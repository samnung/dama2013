/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05)
 *
 * Popis: metody pro síťovou komunikaci
 *
 * encoding: utf-8
 ******************************************************************************/

#include "model_serverplayer.h"

#include <QTimer>
#include <QTcpServer>
#include <QTcpSocket>
#include <QStringList>


ServerPlayer::ServerPlayer(QString port, AbstractController * controller, QObject * parent)
	: NetworkPlayer(port, controller, parent)
{
	_socket = NULL;
}

void ServerPlayer::connectIfNeeded()
{
	_server = new QTcpServer();

	int port;

	if ( _address.contains(':') )
	{
		QStringList list = _address.split(':');
		port = list[1].toInt();
	}
	else
	{
		port = _address.toInt();
	}

	_server->setMaxPendingConnections(1);
	bool listen = _server->listen(QHostAddress::Any, port);

	connect(_server, SIGNAL(newConnection()), this, SLOT(newConnection()));

	if ( ! listen )
	{
		emit connectionRefused();
		return;
	}
	else
	{
		std::cerr << "listening on port: " << port << "\n";
	}
}


void ServerPlayer::newConnection()
{
	if ( ! _socket )
	{
		_socket = _server->nextPendingConnection();
		connect(_socket, SIGNAL(readyRead()), this, SLOT(readyForRead()));

		emit connectionEstablished();
	}
}
