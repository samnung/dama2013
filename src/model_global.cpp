
/*******************************************************************************
 * Projekt: dama2013 (předmět ICP na FIT VUT Brno)
 *
 * Přispěvatelé: Roman Kříž (xkrizr05), Vojtěch Dvořáček (xdvora0y)
 *
 * Popis: globalni operace vyuzivane modelem
 *
 * encoding: utf-8
 ******************************************************************************/
#include "model_global.h"


Color inverseColor(Color color)
{
	return ((color == ColorWhite) ? ColorBlack : ColorWhite);
}

const std::string & colorToString(Color color)
{
	return ((color == ColorWhite) ? ColorWhiteString : ColorBlackString);
}

Color colorFromString(const std::string &str)
{
	return ((str == ColorBlackString) ? ColorBlack : ColorWhite);
}
