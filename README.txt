Na tomto projektu se ucastnili:
	Roman Kriz (xkrizr05)
	Vojtech Dvoracek (xdvora0y)


Implentovana hra dama za pomoci knihovny Qt. Pro vykreslovani se pouzil jediny
obrazek (pro zobrazeni damy), vse ostatni je vykreslovano dynamicky. Muzou hrat
dva umeli hraci proti sobe, nebo pres sit.

CLI aplikace je jen pro "demonstracni", dokaze se hrat jen clovek proti cloveku.
Spusti se pomoci `make run_cli'.

Pro presun figurek, se da pouzit klikani na pocatecni pozici a pote na koncovou
pozici. Nebo jednoduse chytnout figurku a polozit na dane misto (Drag & Drop).
Pri testovani jsem zjistil, ze zpusob pretahnuti je mnohem intuitivnejsi, ale
hlavne prijemnejsi a rychlejsi.

AI funguje hodne primitivne, jednoduse vybere nahodny tah, ktery je dostupny.
Pro zpestreni jsme udelali ruznou prodlevu pomoci casovace (0.5 - 2 s).

Pri prehravacim modu (v menu), se prehrava pomoci tlacitek dole a pro ovladani
rychlost se pouzije posuvnik (nalevo nejpomalejsi, napravo nejrychlejsi),
pokud se chcete premistit na dany tah v histori, staci kliknout na policko
v tabulce (nefunguje kliknuti na hlavicku radku).



Ze zacatku jsem pouzivali nami vytvorene unit testy a mechanizmy pro spousteni,
ktere ale po pridani Qt prestaly fungovat. Museli by se pridat cesty a dalsi veci,
ale kvuli casove tisni jsme vypustili.

