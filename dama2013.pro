#-------------------------------------------------
#
# Project created by QtCreator 2013-04-05T09:20:17
#
#-------------------------------------------------

QT			+= core gui network xml
TARGET		= dama2013
TEMPLATE	= app

HOSTNAME = $$system(hostname)

contains(HOSTNAME, "merlin.fit.vutbr.cz") {
	QMAKE_CXX	= g++-4.7
	QMAKE_CC	= $$QMAKE_CXX
	QMAKE_LINK	= $$QMAKE_CXX
}



macx {

	QMAKE_CXX	= g++-4.8
	QMAKE_CC	= $$QMAKE_CXX
	QMAKE_LINK	= $$QMAKE_CXX

	QMAKE_CXXFLAGS  += -mmacosx-version-min=10.7
}


greaterThan(QT_MAJOR_VERSION, 4): {
	QT += widgets
}


QMAKE_CXXFLAGS += -std=c++11



SOURCES += \
	src/gui_main.cpp \
	src/model_unit.cpp \
	src/model_tile.cpp \
	src/model_board.cpp \
	src/gui_mainwindow.cpp \
	src/gui_board.cpp \
	src/gui_tile.cpp \
	src/gui_unit.cpp \
	src/model_move.cpp \
	src/model_player.cpp \
	src/gui_newgame.cpp \
	src/model_networkplayer.cpp \
	src/model_aiplayer.cpp \
	src/model_serverplayer.cpp \
    src/model_global.cpp \
    src/gui_standardinputdialog.cpp

HEADERS  += \
	src/model_unit.h \
	src/model_tile.h \
	src/model_global.h \
	src/model_board.h \
	src/gui_mainwindow.h \
	src/gui_board.h \
	src/gui_tile.h \
	src/gui_unit.h \
	src/model_move.h \
	src/model_player.h \
	src/gui_newgame.h \
	src/model_abstractcontroller.h \
	src/model_networkplayer.h \
	src/model_aiplayer.h \
	src/model_serverplayer.h \
    src/gui_standardinputdialog.h

FORMS    += \
	src/gui_mainwindow.ui \
	src/gui_newgame.ui \
    src/gui_standardinputdialog.ui

RESOURCES += \
	gui_res.qrc
